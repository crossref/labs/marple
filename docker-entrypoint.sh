case $APPLICATION in
  "api")
    echo "running $APPLICATION"
    POPULATE_DB=1 uvicorn matching.app:app --host 0.0.0.0 --port 8000
    ;;
  "indexer")
    echo "running $APPLICATION"
    python3 indexes/preprint_matching/indexer_service.py
    ;;
  *)
    POPULATE_DB=1 uvicorn matching.app:app --host 0.0.0.0 --port 8000
esac

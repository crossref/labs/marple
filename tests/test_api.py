from fastapi.testclient import TestClient

from matching.app import app
from matching import crud
from matching import models

from strategies_available.preprint_sbmv.strategy import Strategy


client = TestClient(app)


# Mock objects

task_1 = models.Task(id="task_1", description="description_1")
task_2 = models.Task(id="task_2", description="description_2")

strategy_1 = models.Strategy(
    id="strategy_1",
    description="description_1",
    default=True,
    code_module="preprint_sbmv",
    task_id="task_1",
)
strategy_2 = models.Strategy(
    id="strategy_2",
    description="description_2",
    default=False,
    code_module="reference_cs_combined",
    task_id="task_1",
)

data_point_1 = models.DataPoint(
    id="data_point_1",
    seq_no=0,
    input="input_1",
    output=["output_1"],
    dataset_id="dataset_1",
)
data_point_2 = models.DataPoint(
    id="data_point_2",
    seq_no=1,
    input="input_2",
    output=["output_2"],
    dataset_id="dataset_1",
)

dataset_1 = models.Dataset(
    id="dataset_1",
    collected_date="2023-02-27",
    task_id="task_1",
    data_points=[data_point_1, data_point_2],
)
dataset_2 = models.Dataset(
    id="dataset_2", collected_date="2023-04-01", task_id="task_1"
)

result_1 = models.Result(
    id="result_1",
    matched=["output_1"],
    result_set_id="result_set_1",
    data_point_id="data_point_1",
    data_point=data_point_1,
)
result_2 = models.Result(
    id="result_2",
    matched=["output_3"],
    result_set_id="result_set_1",
    data_point_id="data_point_2",
    data_point=data_point_2,
)

result_set_1 = models.ResultSet(
    id="result_set_1",
    matched_date="2023-05-05",
    dataset_id="dataset_1",
    strategy_id="strategy_1",
    strategy=strategy_1,
    results=[result_1, result_2],
)
result_set_2 = models.ResultSet(
    id="result_set_2",
    matched_date="2023-05-10",
    dataset_id="dataset_2",
    strategy_id="strategy_1",
    strategy=strategy_1,
    results=[],
)

strategy_1.result_sets = [result_set_1, result_set_2]


# Mock functions


def mock_get_tasks(db):
    return [task_1, task_2]


def mock_get_task(db, task_id):
    match task_id:
        case "task_1":
            return task_1
        case "task_2":
            return task_2
        case _:
            return None


def mock_get_strategies(db, task_id):
    if task_id == "task_1":
        return [strategy_1, strategy_2]
    return []


def mock_get_strategy(db, task_id=None, strategy_id=None):
    if strategy_id is None:
        if task_id == "task_1":
            return strategy_1
    else:
        match strategy_id:
            case "strategy_1":
                return strategy_1
            case "strategy_2":
                return strategy_2
    return None


def mock_get_dataset(db, dataset_id):
    match dataset_id:
        case "dataset_1":
            return dataset_1
        case "dataset_2":
            return dataset_2
        case _:
            return None


def mock_get_datasets(db, task_id):
    if task_id == "task_1":
        return [dataset_1, dataset_2]
    return []


def mock_get_result_set(db, result_set_id):
    match result_set_id:
        case "result_set_1":
            return result_set_1
        case "result_set_2":
            return result_set_2
        case _:
            return None


# Tests


def test_heartbeat():
    response = client.get("/heartbeat")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}


def test_nonexistent_route():
    response = client.get("/someroute")
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_tasks(monkeypatch):
    monkeypatch.setattr(crud, "get_tasks", mock_get_tasks)

    response = client.get("/tasks")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "task-list"
    assert body["message"]["items"] == [
        {"id": "task_1", "description": "description_1"},
        {"id": "task_2", "description": "description_2"},
    ]


def test_strategies(monkeypatch):
    monkeypatch.setattr(crud, "get_task", mock_get_task)
    monkeypatch.setattr(crud, "get_strategies", mock_get_strategies)

    response = client.get("/tasks/some_task/strategies")
    assert response.status_code == 404
    assert response.json() == {"detail": "No such matching task"}

    response = client.get("/tasks/task_1/strategies")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "strategy-list"
    assert body["message"]["items"] == [
        {
            "id": "strategy_1",
            "description": "description_1",
            "default": True,
        },
        {
            "id": "strategy_2",
            "description": "description_2",
            "default": False,
        },
    ]


def test_datasets(monkeypatch):
    monkeypatch.setattr(crud, "get_task", mock_get_task)
    monkeypatch.setattr(crud, "get_datasets", mock_get_datasets)

    response = client.get("/tasks/some_task/datasets")
    assert response.status_code == 404
    assert response.json() == {"detail": "No such matching task"}

    response = client.get("/tasks/task_1/datasets")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "dataset-list"
    assert body["message"]["items"] == [
        {"id": "dataset_1", "collected-date": "2023-02-27", "task-id": "task_1"},
        {"id": "dataset_2", "collected-date": "2023-04-01", "task-id": "task_1"},
    ]


def test_dataset(monkeypatch):
    monkeypatch.setattr(crud, "get_dataset", mock_get_dataset)

    response = client.get("/datasets/some_dataset")
    assert response.status_code == 404
    assert response.json() == {"detail": "No such dataset"}

    response = client.get("/datasets/dataset_1")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "dataset"
    assert body["message"] == {
        "id": "dataset_1",
        "collected-date": "2023-02-27",
        "task-id": "task_1",
        "data-points": [
            {"input": "input_1", "output": ["output_1"]},
            {"input": "input_2", "output": ["output_2"]},
        ],
    }


def test_result_sets(monkeypatch):
    monkeypatch.setattr(crud, "get_strategy", mock_get_strategy)

    response = client.get("/strategies/some_strategy/resultsets")
    assert response.status_code == 404
    assert response.json() == {"detail": "No such strategy"}

    response = client.get("/strategies/strategy_1/resultsets")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "result-set-list"
    assert body["message"]["items"] == [
        {
            "dataset-id": "dataset_1",
            "id": "result_set_1",
            "matched-date": "2023-05-05",
            "statistics": {
                "false-negatives": 1,
                "false-positives": 1,
                "precision": 0.5,
                "recall": 0.5,
                "total": 2,
                "f05": 0.5,
                "f1": 0.5,
            },
            "strategy-id": "strategy_1",
            "task-id": "task_1",
        },
        {
            "dataset-id": "dataset_2",
            "id": "result_set_2",
            "matched-date": "2023-05-10",
            "statistics": {
                "false-negatives": 0,
                "false-positives": 0,
                "precision": 1.0,
                "recall": 1.0,
                "total": 0,
                "f05": 1.0,
                "f1": 1.0,
            },
            "strategy-id": "strategy_1",
            "task-id": "task_1",
        },
    ]


def test_result_set(monkeypatch):
    monkeypatch.setattr(crud, "get_result_set", mock_get_result_set)

    response = client.get("/resultsets/some_result_set")
    assert response.status_code == 404
    assert response.json() == {"detail": "No such result set"}

    response = client.get("/resultsets/result_set_1")
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "result-set"
    assert body["message"] == {
        "id": "result_set_1",
        "dataset-id": "dataset_1",
        "task-id": "task_1",
        "strategy-id": "strategy_1",
        "matched-date": "2023-05-05",
        "results": [
            {"input": "input_1", "matched": ["output_1"], "output": ["output_1"]},
            {"input": "input_2", "matched": ["output_3"], "output": ["output_2"]},
        ],
        "statistics": {
            "false-negatives": 1,
            "false-positives": 1,
            "precision": 0.5,
            "recall": 0.5,
            "total": 2,
            "f05": 0.5,
            "f1": 0.5,
        },
    }


def test_match(monkeypatch):
    def mock_match(self, input_data):
        return [{"id": "matched_1", "confidence": 0.9, "strategies": ["strategy"]}]

    monkeypatch.setattr(crud, "get_task", mock_get_task)
    monkeypatch.setattr(crud, "get_strategy", mock_get_strategy)
    monkeypatch.setattr(Strategy, "match", mock_match)

    response = client.get("/match")
    assert response.status_code == 422

    response = client.get("/match", params={"task": "task_123", "input": "input"})
    assert response.status_code == 404
    assert response.json() == {"detail": "No such matching task"}

    response = client.get(
        "/match",
        params={"task": "task_1", "strategy": "strategy_123", "input": "input"},
    )
    assert response.status_code == 404
    assert response.json() == {"detail": "Cannot find the strategy"}

    response = client.get(
        "/match", params={"task": "task_1", "strategy": "strategy_2", "input": "input"}
    )
    assert response.status_code == 400
    assert response.json() == {"detail": "Strategy is not implemented"}

    response = client.get("/match", params={"task": "task_1", "input": "input"})
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "matched-item-list"
    assert body["message"]["items"] == [
        {"id": "matched_1", "confidence": 0.9, "strategies": ["strategy"]}
    ]

    response = client.get(
        "/match", params={"task": "task_1", "strategy": "strategy_1", "input": "input"}
    )
    body = response.json()
    assert response.status_code == 200
    assert body["message-type"] == "matched-item-list"
    assert body["message"]["items"] == [
        {"id": "matched_1", "confidence": 0.9, "strategies": ["strategy"]}
    ]

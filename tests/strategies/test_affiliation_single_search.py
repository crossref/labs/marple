from strategies_available.affiliation_single_search.strategy import Strategy


def test_normalize():
    normalized = [
        ("Universidade Estadual Paulista", "universidade estadual paulista"),
        ("Toyohashi Univ. of Tech.", "toyohashi university of technology"),
        ("Muroran Inst. of Technol", "muroran institute of technol"),
        (
            "The Jefferiss Wing, Imperial College Healthcare NHS Trust, St Mary's Hospital, London, UK",
            "jefferiss wing, imperial college healthcare nhs trust, st mary's hospital, london, uk",
        ),
        (
            "Antai College of Economics & Management, Shanghai Jiao Tong University, Shanghai, People's Republic of China",
            "antai college of economics and management, shanghai jiao tong university, shanghai, people's republic of china",
        ),
    ]

    s = Strategy()
    for i, n in normalized:
        assert s.normalize(i) == n


def test_get_countries():
    countries = [
        ("", set()),
        (
            "Biological Research Laboratories, Nissan Chemical Industries, Ltd., 1470 Shiraoka, Shiraoka, Saitama 349-0294, Japan",
            {"JP"},
        ),
        (
            "Specialist Trainee,Department of Neurology,Royal Hallamshire Hospital,Sheffield S10 2JF, UK.",
            {"GB-UK"},
        ),
        (
            "Antai College of Economics & Management, Shanghai Jiao Tong University, Shanghai, People's Republic of China",
            {"CN-HK-TW"},
        ),
        (
            "From the Institute for Genetics and the Department of Internal Medicine I, University of Cologne, Germany; Department of Pathology, University of Frankfurt, Frankfurt/Main, Germany; and the Institute of Cancer Genetics, Columbia University, New York, NY.",
            {"DE", "US-PR"},
        ),
    ]

    s = Strategy()
    for a, c in countries:
        assert set(s.get_countries(a)) == c


def test_score():
    s = Strategy()

    aff = "Department of Neurology,Royal Hallamshire Hospital,Sheffield S10 2JF, UK"
    scores = [
        s.CandidateMatch(
            candidate={"_source": {"names": [{"name": "Royal Hallamshire Hospital"}]}},
            name="Royal Hallamshire Hospital",
            score=100.0,
            start=24,
            end=50,
        ),
        s.CandidateMatch(
            candidate={"_source": {"names": [{"name": "University Hospital"}]}},
            name="",
            score=0,
            start=-1,
            end=-1,
        ),
        s.CandidateMatch(
            candidate={"_source": {"names": [{"name": "U H"}]}},
            name="",
            score=0,
            start=-1,
            end=-1,
        ),
        s.CandidateMatch(
            candidate={"_source": {"names": [{"name": "Univ"}]}},
            name="",
            score=0,
            start=-1,
            end=-1,
        ),
    ]

    for score in scores:
        assert s.score(aff, score.candidate) == score


def test_is_better():
    aff = "Antai College of Economics & Management, Shanghai Jiao Tong University, Shanghai, People's Republic of China"
    s = Strategy()

    assert not s.is_better(
        aff,
        s.CandidateMatch(
            candidate=None,
            name="Shanghai Jiao Tong University",
            score=100.0,
            start=41,
            end=70,
        ),
        s.CandidateMatch(
            candidate=None,
            name="Shanghai Jiao Tong University",
            score=100.0,
            start=41,
            end=70,
        ),
    )

    assert s.is_better(
        aff,
        s.CandidateMatch(
            candidate=None,
            name="Shanghai Jiao Tong University",
            score=100.0,
            start=41,
            end=70,
        ),
        s.CandidateMatch(
            candidate=None,
            name="Antai College of Economics & Management",
            score=100.0,
            start=0,
            end=39,
        ),
    )

    assert s.is_better(
        aff,
        s.CandidateMatch(
            candidate=None,
            name="Shanghai Jiao Tong University",
            score=100.0,
            start=41,
            end=70,
        ),
        s.CandidateMatch(
            candidate=None,
            name="Shandong Jiao Tong University",
            score=95.0,
            start=41,
            end=70,
        ),
    )

    assert s.is_better(
        aff,
        s.CandidateMatch(
            candidate=None,
            name="Antai College of Economics & Management",
            score=100.0,
            start=0,
            end=39,
        ),
        s.CandidateMatch(
            candidate=None,
            name="Antai College of Economics",
            score=100.0,
            start=0,
            end=26,
        ),
    )


def test_last_non_overlapping():
    s = Strategy()
    assert s.last_non_overlapping(
        [
            s.CandidateMatch(
                candidate={"_id": "id1"}, name="name", score=100.0, start=0, end=26
            ),
            s.CandidateMatch(
                candidate={"_id": "id2"}, name="name", score=100.0, start=0, end=40
            ),
            s.CandidateMatch(
                candidate={"_id": "id3"}, name="name", score=100.0, start=55, end=70
            ),
            s.CandidateMatch(
                candidate={"_id": "id4"}, name="name", score=100.0, start=81, end=100
            ),
            s.CandidateMatch(
                candidate={"_id": "id5"}, name="name", score=100.0, start=90, end=106
            ),
        ]
    ) == s.CandidateMatch(
        candidate={"_id": "id3"}, name="name", score=100.0, start=55, end=70
    )

from pytest import approx

from strategies_available.preprint_sbmv_es.strategy import Strategy


def always(value):
    def always_fun(*args):
        return value

    return always_fun


def test_candidate_query():
    s = Strategy()
    assert (
        s.candidate_query(
            {
                "title": ["My Interesting Title"],
                "author": [
                    {"given": "Matt D.H.", "family": "Smith"},
                    {"given": "Tess", "family": "Seal"},
                ],
                "issued": {"date-parts": [[2018]]},
            }
        )
        == "My Interesting Title Smith Seal"
    )


def test_score(monkeypatch):
    monkeypatch.setattr(Strategy, "authors_score", always(0.1))
    monkeypatch.setattr(Strategy, "title_score", always(0.4))
    monkeypatch.setattr(Strategy, "year_score", always(0.7))

    s = Strategy()
    assert round(s.score({}, {}), 1) == 0.4


def test_year_score():
    s = Strategy()
    assert s.year_score({"issued": {"date-parts": [[2015]]}}, {"issued": [2018]}) == 0.0
    assert s.year_score({"issued": {"date-parts": [[2017]]}}, {"issued": [2018]}) == 1.0
    assert s.year_score({"issued": {"date-parts": [[2018]]}}, {"issued": [2018]}) == 1.0
    assert s.year_score({"issued": {"date-parts": [[2021]]}}, {"issued": [2018]}) == 1.0
    assert s.year_score({"issued": {"date-parts": [[2022]]}}, {"issued": [2018]}) == 0.0


def test_title_score():
    s = Strategy()
    assert s.title_score({"title": []}, {"title": []}) == 0.0
    assert s.title_score({"title": ["TITLE"]}, {"title": [" Title  "]}) == 1.0
    assert s.title_score({"title": ["Detailed Title"]}, {"title": ["Title"]}) == 1.0
    assert (
        approx(
            s.title_score({"title": ["Correction: Title"]}, {"title": ["Title"]}),
            abs=1e-3,
        )
        == 0.6667
    )


def test_authors_score():
    s = Strategy()
    assert s.authors_score({"author": []}, {"author": []}) == 0.5
    assert (
        s.authors_score(
            {"author": []},
            {"author": [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}]},
        )
        == 0.0
    )
    assert (
        s.authors_score(
            {"author": [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}]},
            {"author": [{"ORCID": "http://orcid.org/0000-0000-2222-0000"}]},
        )
        == 0.0
    )
    assert (
        s.authors_score({"author": [{"ORCID": ""}]}, {"author": [{"ORCID": ""}]}) == 0.0
    )
    assert (
        s.authors_score(
            {"author": [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}]},
            {"author": [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}]},
        )
        == 1.0
    )
    assert (
        s.authors_score(
            {"author": [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}]},
            {"author": [{"ORCID": "0000-0000-1111-0000"}]},
        )
        == 1.0
    )
    assert (
        s.authors_score(
            {
                "author": [
                    {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
                    {"ORCID": "http://orcid.org/0000-0000-2222-0000"},
                ]
            },
            {
                "author": [
                    {"ORCID": "http://orcid.org/0000-0000-3333-0000"},
                    {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
                    {"ORCID": "http://orcid.org/0000-0000-4444-0000"},
                ]
            },
        )
        == 0.4
    )


def test_most_similar_pair():
    s = Strategy()
    assert s.most_similar_pair([], []) == (0.0, 0, 0)
    assert s.most_similar_pair(
        [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}], []
    ) == (0.0, 0, 0)
    assert s.most_similar_pair(
        [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}],
        [{"ORCID": "http://orcid.org/0000-0000-2222-0000"}],
    ) == (0.0, 0, 0)
    assert s.most_similar_pair(
        [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}],
        [{"ORCID": "http://orcid.org/0000-0000-1111-0000"}],
    ) == (1.0, 0, 0)
    assert s.most_similar_pair(
        [{"name": "Consortium"}, {"ORCID": "http://orcid.org/0000-0000-1111-0000"}],
        [
            {"given": "Matt D.H.", "family": "Smith"},
            {"ORCID": "http://orcid.org/0000-0000-2222-0000"},
            {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
        ],
    ) == (1.0, 1, 2)


def test_score_author_similarity():
    s = Strategy()
    assert s.score_author_similarity({}, {}) == 0.0
    assert (
        s.score_author_similarity(
            {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
            {"ORCID": "http://orcid.org/0000-0000-2222-0000"},
        )
        == 0.0
    )
    assert (
        s.score_author_similarity(
            {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
            {"ORCID": "http://orcid.org/0000-0000-1111-0000"},
        )
        == 1.0
    )
    assert s.score_author_similarity({}, {"name": "Consortium"}) == 0.0
    assert (
        s.score_author_similarity({"name": " Consortium  "}, {"name": "Consortium"})
        == 1.0
    )
    assert (
        s.score_author_similarity(
            {"given": "Matt D.H.", "family": "Smith"},
            {"given": "Matt H.", "family": "Smith"},
        )
        == 1.0
    )
    assert (
        s.score_author_similarity(
            {"given": "Matt D.H.", "family": "Smith"},
            {"given": "Smith", "family": "Matt"},
        )
        == 1.0
    )
    assert (
        0.0
        < s.score_author_similarity(
            {"given": "Matt D.H.", "family": "Smith"},
            {"given": "Tess", "family": "Seal"},
        )
        < 1.0
    )


def test_author_names():
    s = Strategy()
    assert s.author_names({}) == set()
    assert s.author_names({"name": ""}) == set()
    assert s.author_names({"name": "Consortium"}) == {"consortium"}
    assert s.author_names({"name": " Consortium  "}) == {"consortium"}
    assert s.author_names({"given": "Matt D.H.", "family": "Smith", "name": "  "}) == {
        "matt d.h. smith",
        "smith matt d.h.",
        "matt smith",
        "smith matt",
    }
    assert s.author_names(
        {"given": "Matt D.H.", "family": "Smith", "name": "Consortium"}
    ) == {
        "consortium",
        "matt d.h. smith",
        "smith matt d.h.",
        "matt smith",
        "smith matt",
    }

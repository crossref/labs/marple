import json
import os

from jsonschema import validate


def test_datasets_schema():
    datasets_schema = {
        "type": "object",
        "properties": {
            "id": {"type": "string"},
            "task_id": {"type": "string"},
            "collected_date": {"type": "string"},
            "items": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "seq_no": {"type": "integer"},
                        "input": {"type": "string"},
                        "output": {
                            "type": "array",
                            "items": {
                                "type": "string",
                                "oneOf": [
                                    {"pattern": "^https://doi.org/"},
                                    {"pattern": "^https://ror.org/"},
                                ],
                            },
                        },
                    },
                    "required": ["seq_no", "input", "output"],
                    "additionalProperties": False,
                },
            },
        },
        "required": ["id", "task_id", "collected_date", "items"],
        "additionalProperties": False,
    }

    for filename in os.listdir("data/datasets"):
        with open(f"data/datasets/{filename}", "r") as f:
            dataset = json.load(f)
        validate(instance=dataset, schema=datasets_schema)

    for filename in os.listdir("data/training"):
        with open(f"data/training/{filename}", "r") as f:
            dataset = json.load(f)
        validate(instance=dataset, schema=datasets_schema)


def test_resultsets_schema():
    resultsets_schema = {
        "type": "object",
        "properties": {
            "id": {"type": "string"},
            "strategy_id": {"type": "string"},
            "dataset_id": {"type": "string"},
            "matched_date": {"type": "string"},
            "items": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "seq_no": {"type": "integer"},
                        "matched": {
                            "type": "array",
                            "items": {
                                "type": "string",
                                "oneOf": [
                                    {"pattern": "^https://doi.org/"},
                                    {"pattern": "^https://ror.org/"},
                                ],
                            },
                        },
                    },
                    "required": ["seq_no", "matched"],
                    "additionalProperties": False,
                },
            },
        },
        "required": ["id", "strategy_id", "dataset_id", "matched_date", "items"],
        "additionalProperties": False,
    }

    for filename in os.listdir("data/results"):
        with open(f"data/results/{filename}", "r") as f:
            resultset = json.load(f)
        validate(instance=resultset, schema=resultsets_schema)

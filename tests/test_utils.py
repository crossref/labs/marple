from matching.schemas import to_kebab
from matching.utils import doi_id


def test_doi_id():
    assert doi_id(None) is None
    assert doi_id("10.1111/doi") == "https://doi.org/10.1111/doi"


def test_to_kebab():
    assert to_kebab(None) is None
    assert to_kebab("this_is_a_name") == "this-is-a-name"

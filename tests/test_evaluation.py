from pytest import approx

from matching.evaluation import true_links, matched_links, evaluate


def test_true_links():
    assert true_links([]) == set()
    assert true_links([{"output": []}]) == set()
    assert true_links([{"output": ["q"]}]) == set([(0, "q")])
    assert true_links([{"output": ["q", "w"]}]) == set([(0, "q"), (0, "w")])
    assert true_links(
        [{"output": ["q", "w"]}, {"output": []}, {"output": ["e"]}]
    ) == set([(0, "q"), (0, "w"), (2, "e")])
    assert true_links([{"output": ["Q"]}]) == set([(0, "q")])


def test_matched_links():
    assert matched_links([]) == set()
    assert matched_links([{"matched": []}]) == set()
    assert matched_links([{"matched": ["q"]}]) == set([(0, "q")])
    assert matched_links([{"matched": ["q", "w"]}]) == set([(0, "q"), (0, "w")])
    assert matched_links(
        [{"matched": ["q", "w"]}, {"matched": []}, {"matched": ["e"]}]
    ) == set([(0, "q"), (0, "w"), (2, "e")])
    assert matched_links([{"matched": ["Q"]}]) == set([(0, "q")])


def test_evaluate():
    assert evaluate([]) == {
        "total": 0,
        "false-positives": 0,
        "false-negatives": 0,
        "precision": 1.0,
        "recall": 1.0,
        "f05": 1.0,
        "f1": 1.0,
    }
    assert evaluate([{"output": [], "matched": []}]) == {
        "total": 1,
        "false-positives": 0,
        "false-negatives": 0,
        "precision": 1.0,
        "recall": 1.0,
        "f05": 1.0,
        "f1": 1.0,
    }
    assert evaluate([{"output": ["a"], "matched": []}]) == {
        "total": 1,
        "false-positives": 0,
        "false-negatives": 1,
        "precision": 1.0,
        "recall": 0.0,
        "f05": 0.0,
        "f1": 0.0,
    }
    assert evaluate([{"output": [], "matched": ["a"]}]) == {
        "total": 1,
        "false-positives": 1,
        "false-negatives": 0,
        "precision": 0.0,
        "recall": 1.0,
        "f05": 0.0,
        "f1": 0.0,
    }
    assert evaluate([{"output": ["a"], "matched": ["a"]}]) == {
        "total": 1,
        "false-positives": 0,
        "false-negatives": 0,
        "precision": 1.0,
        "recall": 1.0,
        "f05": 1.0,
        "f1": 1.0,
    }
    assert approx(evaluate([{"output": ["a", "b"], "matched": ["a"]}]), abs=1e-3) == {
        "total": 1,
        "false-positives": 0,
        "false-negatives": 1,
        "precision": 1.0,
        "recall": 0.5,
        "f05": 0.8333,
        "f1": 0.6667,
    }
    assert approx(evaluate([{"output": ["a"], "matched": ["a", "b"]}]), abs=1e-3) == {
        "total": 1,
        "false-positives": 1,
        "false-negatives": 0,
        "precision": 0.5,
        "recall": 1.0,
        "f05": 0.5556,
        "f1": 0.6667,
    }
    assert evaluate([{"output": ["a", "c"], "matched": ["a", "b"]}]) == {
        "total": 1,
        "false-positives": 1,
        "false-negatives": 1,
        "precision": 0.5,
        "recall": 0.5,
        "f05": 0.5,
        "f1": 0.5,
    }

# Marple

Marple is a prototype matching service that allows to:
* run multiple matching tasks
* implement multiple matching strategies
* create and populate backend indexes for the matching strategies
* evaluate the strategies against ground truth datasets
* view available datasets and evaluation results

The prototype service is available at https://marple.research.crossref.org/docs

## Terminology

In short, matching is the task or process of finding an identifier of an item based on a structured or unstructured “description” of it. Examples include:
* finding the DOI based on a bibliographic reference,
* finding the ROR ID based on an affiliation string,
* finding the grant DOI based on the acknowledgment section of a paper.

There are also tasks that are technically not matching but are closely related, and in practice, we often treat them as matching tasks:
* finding a duplicate of a journal article,
* linking preprints to journal articles.

And there are a few tasks that are often included in matching conversations, but are definitely not matching, for example:
* retrieving the metadata of a work based on its DOI,
* retrieving all works that contain the phrase “citation parsing” in the title.

A matching task defines the nature of matching. Example matching tasks are bibliographic reference matching, preprint matching, and affiliation matching. A matching task has input and output:
* Input is all the data needed for the matching, for example: a structured record or a list of them, unstructured text.
* Output are simply matched identifiers. Within a specific matching task, output identifiers are usually of a specific type (i.e. we match to ROR ID, and not ORCID ID). In some cases, there can be a certain target database as well (i.e. we match only to DataCite DOIs). The output identifiers can have different cardinality depending on the task, some matching tasks will allow zero, one, and/or more identifiers as a result of matching of a single input.

A matching strategy defines how the matching is actually done. Multiple strategies can exist for a specific matching task. Some strategies can even run other strategies and combine their outcomes.

We can evaluate a matching strategy using an evaluation set. An evaluation set is composed of data points. Such a data point contains:
* input that can be passed to the strategy,
* expected (true) output (a set of identifiers).

A result set is a result of applying a strategy to an evaluation set. A result data point contains:
* input that was passed to the strategy,
* matched output, as returned from the strategy (a set of identifiers).

During the evaluation, we compare the expected outputs with the matched outputs and use the evaluation metrics to calculate how well the strategy did on the evaluation set.

## Development

### Database

Marple depends on a Postgres database to store the information about the supported tasks, available strategies, datasets and evaluation results. Use the following env vars to point Marple to the database:
* DB_USER
* DB_PASSWORD
* DB_SERVER
* DB_NAME

The table will be created and populated automatically when the service starts.

### Indexes

Some strategies need an Elasticsearch index to match. [indexes directory](indexes) contains scripts for creating and populating the indexes.

Use ES_HOST env var to point Marple to the Elasticsearch cluster.

### How to run

Run

```
POPULATE_DB=1 uvicorn matching.app:app --host 0.0.0.0 --port 8000
```

and then visit http://localhost:8000

### Strategies

New strategies should be added to [strategies_available directory](strategies_available). See existing strategies for examples.

To enable a strategy, create a symlink to the strategy module in [strategies_enabled directory](strategies_enabled).

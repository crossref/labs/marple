import argparse
import json
import requests

from datetime import datetime
from multiprocessing.pool import Pool


def match(data):
    url, task, strategy, data_point = data
    print(f"matching data point {data_point['seq_no']}")
    response = requests.get(
        f"{url}/match",
        {"task": task, "strategy": strategy, "input": data_point["input"]},
    ).json()
    matched = [i["id"] for i in response["message"]["items"]]
    return {"seq_no": data_point["seq_no"], "matched": matched}


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run a matching strategy on a dataset")
    parser.add_argument("-d", "--dataset", help="dataset file", type=str, required=True)
    parser.add_argument(
        "-u",
        "--url",
        help="matching service url",
        type=str,
        default="https://marple.research.crossref.org",
    )
    parser.add_argument("-t", "--task", help="matching task", type=str, required=True)
    parser.add_argument(
        "-s", "--strategy", help="matching strategy", type=str, required=True
    )
    parser.add_argument(
        "-i", "--resultsetid", help="result set id", type=str, required=True
    )
    parser.add_argument("-o", "--output", help="output file", type=str, required=True)
    parser.add_argument(
        "-n", "--threads", help="number of threads", type=int, default=4
    )
    args = parser.parse_args()

    with open(args.dataset, "r") as f:
        dataset = json.load(f)

    resultset = {
        "id": args.resultsetid,
        "strategy_id": args.strategy,
        "dataset_id": dataset["id"],
        "matched_date": datetime.today().strftime("%Y-%m-%d"),
        "items": [],
    }

    with Pool(args.threads) as p:
        args_generator = map(
            lambda r: (args.url, args.task, args.strategy, r), dataset["items"]
        )
        for t in p.imap(match, args_generator, 32):
            resultset["items"].append(t)

    with open(args.output, "w") as f:
        json.dump(resultset, f, indent=2)

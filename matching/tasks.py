TASKS = [
    {
        "id": "reference-matching",
        "description": "Matching bibliographic references to works, such as "
        + "journal articles, conference papers, etc.",
    },
    {
        "id": "preprint-matching",
        "description": "Matching journal articles to preprints.",
    },
    {"id": "affiliation-matching", "description": "Matching affiliations to ROR IDs."},
]

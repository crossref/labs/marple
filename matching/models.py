from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import ARRAY

from matching.database import Base


class Task(Base):
    __tablename__ = "tasks"

    id = Column(String, primary_key=True, index=True)
    description = Column(String)


class Strategy(Base):
    __tablename__ = "strategies"

    id = Column(String, primary_key=True, index=True)
    description = Column(String)
    default = Column(Boolean, nullable=False)
    code_module = Column(String)
    task_id = Column(String, ForeignKey("tasks.id"), nullable=False)

    result_sets = relationship("ResultSet", back_populates="strategy")


class DataPoint(Base):
    __tablename__ = "data_points"

    id = Column(Integer, primary_key=True, index=True)
    seq_no = Column(Integer, nullable=False)
    input = Column(String, nullable=False)
    output = Column(ARRAY(String))
    dataset_id = Column(String, ForeignKey("datasets.id"), nullable=False)


class Dataset(Base):
    __tablename__ = "datasets"

    id = Column(String, primary_key=True, index=True)
    collected_date = Column(Date, nullable=False)
    task_id = Column(String, ForeignKey("tasks.id"), nullable=False)

    data_points = relationship("DataPoint")


class Result(Base):
    __tablename__ = "results"

    id = Column(Integer, primary_key=True, index=True)
    matched = Column(ARRAY(String))
    result_set_id = Column(
        String, ForeignKey("result_sets.id"), nullable=False, index=True
    )
    data_point_id = Column(Integer, ForeignKey("data_points.id"), nullable=False)

    data_point = relationship("DataPoint")


class ResultSet(Base):
    __tablename__ = "result_sets"

    id = Column(String, primary_key=True, index=True)
    matched_date = Column(Date, nullable=False)
    dataset_id = Column(String, ForeignKey("datasets.id"), nullable=False)
    strategy_id = Column(String, ForeignKey("strategies.id"), nullable=False)

    strategy = relationship("Strategy", back_populates="result_sets")
    results = relationship("Result")

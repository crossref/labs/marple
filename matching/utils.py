import json
import requests

from datetime import timedelta
from ratelimit import limits, sleep_and_retry
from starlette.responses import JSONResponse
from typing import Any


@sleep_and_retry
@limits(calls=10, period=timedelta(seconds=1).total_seconds())
def crossref_rest_api_call(route, params):
    params["mailto"] = "dtkaczyk@crossref.org"
    result = requests.get(f"https://api.crossref.org/{route}", params)
    code = result.status_code
    if code == 200:
        result = result.json()["message"]
    return code, result


def doi_id(doi_str):
    if doi_str is None:
        return None
    return f"https://doi.org/{doi_str}"


class AsciiJSONResponse(JSONResponse):
    def render(self, content: Any) -> bytes:
        return json.dumps(content, ensure_ascii=True).encode("utf-8")

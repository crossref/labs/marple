import strategies_available

from matching import crud
from matching import schemas
from matching.database import SessionLocal, populate_db
from matching.evaluation import evaluate
from matching.utils import AsciiJSONResponse

from fastapi import FastAPI, HTTPException, Depends, Request
from typing import Union
from sqlalchemy.orm import Session

import os


strategies = {}

for module_name in os.listdir("strategies_enabled"):
    __import__(f"strategies_available.{module_name}.strategy")
    strategy_module = getattr(strategies_available, module_name)
    strategies[module_name] = strategy_module.strategy.Strategy()

if os.environ.get("POPULATE_DB", ""):
    populate_db()

app = FastAPI(
    title="Crossref Matching API",
    description="Matching API allows to match structured "
    "and unstructured data to identifiers.",
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get(
    "/heartbeat",
    response_model=schemas.HeartbeatResponse,
    tags=["Health"],
    name="",
    description="Heartbeat",
    response_class=AsciiJSONResponse,
)
async def heartbeat():
    return {"status": "ok"}


@app.get(
    "/tasks",
    response_model=schemas.TasksResponse,
    tags=["Tasks and strategies"],
    name="",
    description="The list of supported matching tasks.",
    response_class=AsciiJSONResponse,
)
async def list_tasks(db: Session = Depends(get_db)):
    tasks = crud.get_tasks(db)
    return schemas.TasksResponse(message=schemas.TasksMessage(items=tasks))


@app.get(
    "/tasks/{task_id}/strategies",
    response_model=schemas.StrategiesResponse,
    tags=["Tasks and strategies"],
    name="",
    description="The list of strategies available " "for a given matching tasks.",
    response_class=AsciiJSONResponse,
)
async def list_strategies(task_id: str, db: Session = Depends(get_db)):
    if crud.get_task(db, task_id) is None:
        raise HTTPException(status_code=404, detail="No such matching task")
    strategies = crud.get_strategies(db, task_id)
    return schemas.StrategiesResponse(
        message=schemas.StrategiesMessage(items=strategies)
    )


@app.get(
    "/match",
    response_model=schemas.MatchedItemsResponse,
    tags=["Matching"],
    name="",
    description="Match input to identifiers.",
    response_class=AsciiJSONResponse,
)
async def match(
    task: str,
    input: str,
    strategy: Union[str, None] = None,
    db: Session = Depends(get_db),
):
    if crud.get_task(db, task) is None:
        raise HTTPException(status_code=404, detail="No such matching task")

    strategy = crud.get_strategy(db, task, strategy)
    if strategy is None:
        raise HTTPException(status_code=404, detail="Cannot find the strategy")

    try:
        items = strategies[strategy.code_module].match(input)
        items = [schemas.MatchedItem.parse_obj(i) for i in items]
        return schemas.MatchedItemsResponse(
            message=schemas.MatchedItemsMessage(items=items)
        )
    except NotImplementedError:
        raise HTTPException(
            status_code=400,
            detail="Strategy is not implemented",
        )


@app.post(
    "/match",
    response_model=schemas.MatchedItemsResponse,
    tags=["Matching"],
    name="",
    description="Match input to identifiers.",
    response_class=AsciiJSONResponse,
)
async def match_post(
    request: Request,
    task: str,
    strategy: Union[str, None] = None,
    db: Session = Depends(get_db),
):
    if crud.get_task(db, task) is None:
        raise HTTPException(status_code=404, detail="No such matching task")

    strategy = crud.get_strategy(db, task, strategy)
    if strategy is None:
        raise HTTPException(status_code=404, detail="Cannot find the strategy")

    try:
        items = strategies[strategy.code_module].match(
            (await request.body()).decode("UTF-8")
        )
        items = [schemas.MatchedItem.parse_obj(i) for i in items]
        return schemas.MatchedItemsResponse(
            message=schemas.MatchedItemsMessage(items=items)
        )
    except NotImplementedError:
        raise HTTPException(
            status_code=400,
            detail="Strategy is not implemented",
        )


@app.get(
    "/tasks/{task_id}/datasets",
    response_model=schemas.DatasetsResponse,
    tags=["Evaluation"],
    name="",
    description="List the datasets available for a given matching task.",
    response_class=AsciiJSONResponse,
)
async def list_datasets(task_id: str, db: Session = Depends(get_db)):
    if crud.get_task(db, task_id) is None:
        raise HTTPException(status_code=404, detail="No such matching task")
    datasets = crud.get_datasets(db, task_id)
    return schemas.DatasetsResponse(message=schemas.DatasetsMessage(items=datasets))


@app.get(
    "/datasets/{dataset_id}",
    response_model=schemas.DatasetResponse,
    tags=["Evaluation"],
    name="",
    description="Retrieve the dataset.",
    response_class=AsciiJSONResponse,
)
async def retrieve_dataset(dataset_id: str, db: Session = Depends(get_db)):
    dataset = crud.get_dataset(db, dataset_id)
    if dataset is None:
        raise HTTPException(status_code=404, detail="No such dataset")
    dataset.data_points.sort(key=lambda p: p.seq_no)
    return schemas.DatasetResponse(message=dataset)


@app.get(
    "/resultsets/{result_set_id}",
    response_model=schemas.ResultSetResponse,
    tags=["Evaluation"],
    name="",
    description="Retrieve the result set.",
    response_class=AsciiJSONResponse,
)
async def retrieve_result_set(result_set_id: str, db: Session = Depends(get_db)):
    result_set = crud.get_result_set(db, result_set_id)
    if result_set is None:
        raise HTTPException(status_code=404, detail="No such result set")

    strategy = result_set.strategy
    results = result_set.results
    results.sort(key=lambda r: r.data_point.seq_no)

    result_set = result_set.__dict__
    result_set["results"] = []
    for result in results:
        result_set["results"].append(
            {
                "input": result.data_point.input,
                "output": result.data_point.output,
                "matched": result.matched,
            }
        )
    result_set["task-id"] = strategy.task_id
    result_set["statistics"] = evaluate(result_set["results"])

    return schemas.ResultSetResponse(message=result_set)


@app.get(
    "/strategies/{strategy_id}/resultsets",
    response_model=schemas.ResultSetsResponse,
    tags=["Evaluation"],
    name="",
    description="List the evaluation result sets "
    "available for a given matching strategy.",
    response_class=AsciiJSONResponse,
)
async def list_result_sets(strategy_id: str, db: Session = Depends(get_db)):
    strategy = crud.get_strategy(db, strategy_id=strategy_id)
    if strategy is None:
        raise HTTPException(status_code=404, detail="No such strategy")

    result_sets = []
    for result_set_obj in strategy.result_sets:
        result_set = result_set_obj.__dict__
        results = []
        for result in result_set_obj.results:
            results.append(
                {
                    "input": result.data_point.input,
                    "output": result.data_point.output,
                    "matched": result.matched,
                }
            )
        result_set["task-id"] = strategy.task_id
        result_set["statistics"] = evaluate(results)
        result_sets.append(result_set)

    return schemas.ResultSetsResponse(
        message=schemas.ResultSetsMessage(items=result_sets)
    )

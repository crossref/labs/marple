import boto3
import os

from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth


def get_auth():
    credentials = boto3.Session().get_credentials()
    return AWSV4SignerAuth(credentials, "us-east-1", "es")


ES_CLIENT = None

if os.environ.get("ES_HOST"):
    ES_CLIENT = OpenSearch(
        hosts=[{"host": os.environ.get("ES_HOST"), "port": 443}],
        http_auth=get_auth(),
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )
elif os.environ.get("ES_HOST_DEV"):
    ES_CLIENT = OpenSearch(
        hosts=[
            {
                "host": os.environ.get("ES_HOST_DEV"),
                "port": int(os.environ.get("ES_PORT", "9200")),
            }
        ],
        use_ssl=False,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )

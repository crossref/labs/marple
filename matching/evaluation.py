def true_links(data_points):
    return {(i, d.lower()) for i, p in enumerate(data_points) for d in p["output"]}


def matched_links(data_points):
    return {(i, d.lower()) for i, p in enumerate(data_points) for d in p["matched"]}


def evaluate(data_points):
    tl = true_links(data_points)
    ml = matched_links(data_points)
    precision = len(ml & tl) / len(ml) if ml else 1.0
    recall = len(ml & tl) / len(tl) if tl else 1.0
    return {
        "total": len(data_points),
        "false-positives": len(ml - tl),
        "false-negatives": len(tl - ml),
        "precision": precision,
        "recall": recall,
        "f1": 2.0 * precision * recall / (precision + recall),
        "f05": (1.0 + 0.5 * 0.5)
        * precision
        * recall
        / (0.5 * 0.5 * precision + recall),
    }

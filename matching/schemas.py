from datetime import date
from pydantic import BaseModel


def to_kebab(string: str) -> str:
    if string is None:
        return None
    return "-".join(string.split("_"))


class Item(BaseModel):
    class Config:
        populate_by_name = True
        alias_generator = to_kebab


class OrmItem(BaseModel):
    class Config:
        from_attributes = True
        populate_by_name = True
        alias_generator = to_kebab


class Task(OrmItem):
    id: str
    description: str


class Strategy(OrmItem):
    id: str
    description: str
    default: bool


class MatchedItem(Item):
    id: str
    confidence: float
    strategies: list[str]


class DataPoint(OrmItem):
    input: str
    output: list[str]


class DatasetBasic(OrmItem):
    id: str
    collected_date: date
    task_id: str


class Dataset(DatasetBasic):
    data_points: list[DataPoint]


class Result(Item):
    input: str
    output: list[str]
    matched: list[str]


class Statistics(Item):
    total: int
    false_negatives: int
    false_positives: int
    precision: float
    recall: float
    f1: float
    f05: float


class ResultSetBasic(Item):
    id: str
    task_id: str
    strategy_id: str
    dataset_id: str
    matched_date: date
    statistics: Statistics


class ResultSet(ResultSetBasic):
    results: list[Result]


class TasksMessage(BaseModel):
    items: list[Task]


class StrategiesMessage(BaseModel):
    items: list[Strategy]


class MatchedItemsMessage(BaseModel):
    items: list[MatchedItem]


class DatasetsMessage(BaseModel):
    items: list[DatasetBasic]


class ResultSetsMessage(BaseModel):
    items: list[ResultSetBasic]


class Response(Item):
    status: str = "ok"
    message_version: str = "1.0.0"
    message_type: str


class TasksResponse(Response):
    message_type: str = "task-list"
    message: TasksMessage


class StrategiesResponse(Response):
    message_type: str = "strategy-list"
    message: StrategiesMessage


class MatchedItemsResponse(Response):
    message_type: str = "matched-item-list"
    message: MatchedItemsMessage


class DatasetResponse(Response):
    message_type: str = "dataset"
    message: Dataset


class DatasetsResponse(Response):
    message_type: str = "dataset-list"
    message: DatasetsMessage


class ResultSetResponse(Response):
    message_type: str = "result-set"
    message: ResultSet


class ResultSetsResponse(Response):
    message_type: str = "result-set-list"
    message: ResultSetsMessage


class HeartbeatResponse(BaseModel):
    status: str

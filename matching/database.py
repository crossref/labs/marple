import json
import os

import strategies_available

from matching import models
from matching import tasks

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session


user = os.environ.get("DB_USER")
password = os.environ.get("DB_PASSWORD")
server = os.environ.get("DB_SERVER")
name = os.environ.get("DB_NAME")

SQLALCHEMY_DATABASE_URL = f"postgresql://{user}:{password}@{server}/{name}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def populate_db():
    print("Populating DB")
    models.Base.metadata.drop_all(bind=engine)
    models.Base.metadata.create_all(bind=engine)

    with Session(bind=engine) as session:
        print("Updating tasks")
        for task_cfg in tasks.TASKS:
            task = models.Task(**task_cfg)
            session.merge(task)

        print("Updating strategies")
        for module_name in os.listdir("strategies_enabled"):
            __import__(f"strategies_available.{module_name}.strategy")
            strategy_module = getattr(strategies_available, module_name)
            strategy_class = strategy_module.strategy.Strategy
            strategy = models.Strategy(
                id=strategy_class.strategy,
                task_id=strategy_class.task,
                description=strategy_class.description,
                code_module=module_name,
                default=strategy_class.default,
            )
            session.merge(strategy)
        session.commit()

        print("Updating datasets")
        datasets_dir = "data/datasets"
        for dataset_file in os.listdir(datasets_dir):
            with open(os.path.join(datasets_dir, dataset_file), "r") as f:
                dataset_cfg = json.load(f)
            data_points_cfg = dataset_cfg["items"]
            del dataset_cfg["items"]

            dataset = models.Dataset(**dataset_cfg)
            session.merge(dataset)

            for data_point_cfg in data_points_cfg:
                data_point_cfg["dataset_id"] = dataset_cfg["id"]
                data_point = models.DataPoint(**data_point_cfg)
                session.merge(data_point)

        print("Updating results")
        result_set_dir = "data/results"
        for result_set_file in os.listdir(result_set_dir):
            with open(os.path.join(result_set_dir, result_set_file), "r") as f:
                result_set_cfg = json.load(f)
            results_cfg = result_set_cfg["items"]
            del result_set_cfg["items"]

            if (
                not session.query(models.Strategy)
                .filter(models.Strategy.id == result_set_cfg["strategy_id"])
                .count()
            ):
                print(f"Result set {result_set_cfg} ignored; strategy not found")
                continue

            result_set = models.ResultSet(**result_set_cfg)
            session.merge(result_set)

            for result_cfg in results_cfg:
                data_point_id = (
                    session.query(models.DataPoint)
                    .filter(
                        models.DataPoint.dataset_id == result_set_cfg["dataset_id"],
                        models.DataPoint.seq_no == result_cfg["seq_no"],
                    )
                    .first()
                    .id
                )
                result_cfg["result_set_id"] = result_set_cfg["id"]
                result_cfg["data_point_id"] = data_point_id
                del result_cfg["seq_no"]
                result = models.Result(**result_cfg)
                session.merge(result)

        session.commit()

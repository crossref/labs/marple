from matching import models

from sqlalchemy.orm import Session


def get_tasks(db: Session):
    return db.query(models.Task).all()


def get_task(db: Session, task_id: str):
    return db.query(models.Task).filter(models.Task.id == task_id).first()


def get_strategies(db: Session, task_id: str):
    return db.query(models.Strategy).filter(models.Strategy.task_id == task_id).all()


def get_strategy(db: Session, task_id=None, strategy_id=None):
    q = db.query(models.Strategy)
    if task_id is not None:
        q = q.filter(models.Strategy.task_id == task_id)
    if strategy_id is None:
        q = q.filter(models.Strategy.default)
    else:
        q = q.filter(models.Strategy.id == strategy_id)
    return q.first()


def get_dataset(db: Session, dataset_id: str):
    return db.query(models.Dataset).filter(models.Dataset.id == dataset_id).first()


def get_datasets(db: Session, task_id: str):
    return db.query(models.Dataset).filter(models.Dataset.task_id == task_id).all()


def get_result_set(db: Session, result_set_id: str):
    return (
        db.query(models.ResultSet).filter(models.ResultSet.id == result_set_id).first()
    )

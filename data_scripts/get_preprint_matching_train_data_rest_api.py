import argparse
import json
import requests

from datetime import datetime


def api_request(route="works", params={}):
    response = requests.get(f"https://api.crossref.org/{route}", params)
    if response.status_code != 200:
        return None
    data = response.json()["message"]
    if "items" in data:
        data = data["items"]
    return data


def copy_fields(d, fields):
    copy = {}
    for field in fields:
        if field in d:
            copy[field] = d[field]
    return copy


def article_representation(metadata):
    representation = copy_fields(
        metadata,
        [
            "DOI",
            "references-count",
            "language",
            "title",
            "subtitle",
            "short-title",
            "original-title",
            "issued",
        ],
    )
    if "author" in metadata:
        representation["author"] = [
            copy_fields(author, ["name", "given", "family", "ORCID"])
            for author in metadata["author"]
        ]
    return representation


def get_relation_objects(metadata, rel):
    objects = set()
    for item in metadata.get("relation", {}).get(rel, []):
        objects.add(item["id"].lower())
    return objects


def get_preprints_from_has_preprint(article):
    preprints = set()
    for preprint_candidate in get_relation_objects(article, "has-preprint"):
        preprint = api_request(route=f"works/{preprint_candidate}")
        if (
            preprint is not None
            and preprint["type"] == "posted-content"
            and preprint["subtype"] == "preprint"
        ):
            preprints.add(preprint_candidate)
    return preprints


def get_article_of_preprint(preprint, article_dois):
    if preprint["subtype"] != "preprint":
        return None
    for doi in get_relation_objects(preprint, "is-preprint-of"):
        if doi in article_dois:
            return doi
    return None


def get_preprints(article_dois):
    representations = {}
    preprints = {}

    article_dois_filter = "type:journal-article,"
    article_dois_filter += ",".join([f"doi:{doi}" for doi in article_dois])
    articles_items = api_request(
        params={
            "filter": article_dois_filter,
            "rows": len(article_dois),
        }
    )
    for article in articles_items:
        doi = article["DOI"].lower()
        if doi not in article_dois:
            continue
        representations[doi] = article_representation(article)
        preprints[doi] = get_preprints_from_has_preprint(article)

    preprints_filter = "type:posted-content,relation.type:is-preprint-of,"
    preprints_filter += ",".join([f"relation.object:{doi}" for doi in representations])
    preprints_filter += ","
    preprints_filter += ",".join(
        [f"relation.object:{doi.upper()}" for doi in representations]
    )
    preprint_items = api_request(params={"filter": preprints_filter, "rows": 1000})
    for preprint in preprint_items:
        article_doi = get_article_of_preprint(preprint, representations.keys())
        if article_doi is not None:
            preprints[article_doi].add(preprint["DOI"].lower())

    return preprints, representations


def positive_candidates():
    c_filter = (
        "relation.type:has-preprint,relation.type:is-preprint-of,from-issued-date:2017"
    )
    while True:
        items = api_request(params={"filter": c_filter, "sample": 100})
        for item in items:
            if "journal-article" == item["type"]:
                yield item["DOI"].lower()
            elif "posted-content" == item["type"] and "preprint" == item["subtype"]:
                candidates = get_relation_objects(item, "is-preprint-of")
                if candidates:
                    yield list(candidates)[0]


def negative_candidates():
    c_filter = "type:journal-article,from-issued-date:2017"
    while True:
        items = api_request(params={"filter": c_filter, "sample": 100})
        for item in items:
            if get_relation_objects(item, "has-preprint"):
                continue
            yield item["DOI"].lower()


def get_batches(generator, n=10):
    batch = []
    for candidate in generator:
        batch.append(candidate)
        if len(batch) == n:
            yield batch
            batch = []


def positive_data_points():
    for batch in get_batches(positive_candidates()):
        preprints, representations = get_preprints(batch)
        for article in representations:
            if preprints[article]:
                yield article, representations[article], preprints[article]


def negative_data_points():
    for batch in get_batches(negative_candidates()):
        preprints, representations = get_preprints(batch)
        for article in representations:
            if not preprints[article]:
                yield article, representations[article]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Gather training data for preprint matching from the REST API"
    )
    parser.add_argument("-s", "--size", help="dataset size", type=int, required=True)
    parser.add_argument("-i", "--id", help="result set id", type=str, required=True)
    parser.add_argument(
        "-e", "--exclude", help="dataset to exclude", type=str, default=None
    )
    parser.add_argument("-o", "--output", help="output file", type=str, required=True)
    args = parser.parse_args()

    dataset = {
        "id": args.id,
        "task_id": "preprint-matching",
        "collected_date": datetime.today().strftime("%Y-%m-%d"),
        "items": [],
    }

    exclude = set()

    if args.exclude is not None:
        for filename in args.exclude.split(","):
            with open(filename, "r") as f:
                data = json.load(f)
                exclude.update(
                    [json.loads(item["input"])["DOI"] for item in data["items"]]
                )

    for article_doi, article, preprints in positive_data_points():
        size = len(dataset["items"])
        if size == int(args.size / 2):
            break
        if size % 10 == 0:
            print(size)
        if article_doi in exclude:
            continue
        exclude.add(article_doi)
        dataset["items"].append(
            {
                "seq_no": size,
                "input": json.dumps(article),
                "output": [f"https://doi.org/{d}" for d in preprints],
            }
        )

    for article_doi, article in negative_data_points():
        size = len(dataset["items"])
        if size == args.size:
            break
        if size % 10 == 0:
            print(size)
        if article_doi in exclude:
            continue
        exclude.add(article_doi)
        dataset["items"].append(
            {
                "seq_no": size,
                "input": json.dumps(article),
                "output": [],
            }
        )

    with open(args.output, "w") as f:
        json.dump(dataset, f, indent=2)

import argparse
import boto3
import json
import re

from botocore import UNSIGNED
from botocore.client import Config
from datetime import datetime


def get_keys(paginator, bucket, prefix):
    for page in paginator.paginate(Bucket=bucket, Prefix=prefix):
        for obj in page["Contents"]:
            if obj["Key"].endswith(".jsonl"):
                yield obj["Key"]


def get_jsonl(s3client, bucket, key):
    data = s3client.get_object(Bucket=bucket, Key=key)["Body"].read().decode("utf-8")
    return [json.loads(line) for line in data.split("\n")]


def clean_ref(ref):
    r = re.sub(
        "(?<!\d)10\.\d{4,9}/[-\._;\(\)/:a-zA-Z0-9]+", "", re.sub("\s+", " ", ref)
    )
    r = re.sub("https://doi\.org/", "", r).strip()
    r = re.sub("doi:", "", r).strip()
    return r


def get_references_from_sample(sample):
    references = []
    for item in sample:
        for ref in item["data-point"].get("reference", []):
            if (
                "DOI" in ref
                and "unstructured" in ref
                and "publisher" == ref["doi-asserted-by"]
            ):
                references.append((clean_ref(ref["unstructured"]), ref["DOI"]))
    return references


def get_references(paginator, bucket):
    for key in get_keys(paginator, bucket, "all-works"):
        print("reading", key)
        sample = get_jsonl(s3client, bucket, key)
        references = get_references_from_sample(sample)
        for item in references:
            yield item


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Gather training data from samples")
    parser.add_argument("-s", "--size", help="dataset size", type=int, default=-1)
    parser.add_argument(
        "-i", "--resultsetid", help="result set id", type=str, required=True
    )
    parser.add_argument(
        "-e", "--exclude", help="dataset to exclude", type=str, default=None
    )
    parser.add_argument("-o", "--output", help="output file", type=str, required=True)
    args = parser.parse_args()

    exclude_references = set()
    if args.exclude is not None:
        with open(args.exclude, "r") as f:
            exclude_references = json.load(f)
        exclude_references = set([d["input"] for d in exclude_references["items"]])

    dataset = {
        "id": args.resultsetid,
        "task_id": "reference-matching",
        "matched_date": datetime.today().strftime("%Y-%m-%d"),
        "items": [],
    }

    s3client = boto3.client("s3", config=Config(signature_version=UNSIGNED))
    bucket = "samples.research.crossref.org"
    paginator = s3client.get_paginator("list_objects_v2")

    for ref_string, doi in get_references(paginator, bucket):
        if args.size > 0 and len(dataset["items"]) >= args.size:
            break
        if ref_string in exclude_references:
            continue
        dataset["items"].append(
            {
                "seq_no": len(dataset["items"]),
                "input": ref_string,
                "output": [f"https://doi.org/{doi}"],
            }
        )

    with open(args.output, "w") as f:
        json.dump(dataset, f, indent=2)

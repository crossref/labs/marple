import json
import re
import urllib.parse

from matching.utils import crossref_rest_api_call, doi_id

from strategies_available.preprint_sbmv.strategy import Strategy as SBMV
from strategies_available.preprint_ml.strategy import Strategy as ML


class Strategy:
    strategy = "preprint-ensemble-avg"
    task = "preprint-matching"
    description = (
        "This strategy uses Crossref's REST API to search for candidate "
        + "preprints, and averages scores of SBMV and ML-based strategies."
    )
    default = False

    min_score = 0.75
    max_score_diff = 0.03
    max_query_len = 5000

    def match(self, input_data):
        if input_data.startswith("10."):
            _, article = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(input_data)}", {}
            )
        else:
            article = json.loads(input_data)

        query = self.candidate_query(article)
        code, results = crossref_rest_api_call(
            "works", {"query.bibliographic": query, "filter": "type:posted-content"}
        )
        candidates = [r for r in results["items"] if r.get("subtype", "") == "preprint"]

        strategies = [SBMV(), ML()]
        scores = [(r["DOI"], self.score(article, r, strategies)) for r in candidates]

        matches = [(d, s) for d, s in scores if s >= self.min_score]
        top_score = max([0] + [s for _, s in matches])
        matches = [(d, s) for d, s in matches if top_score - s < self.max_score_diff]

        return [
            {
                "id": doi_id(d),
                "confidence": s,
                "strategies": [self.strategy],
            }
            for d, s in matches
        ]

    def candidate_query(self, article):
        title = " ".join(article["title"])
        title = re.sub("mml:[a-z]*", "", title)
        title = re.sub("&amp;", "&", title)
        title = re.sub("&lt;", "<", title)
        title = re.sub("&gt;", ">", title)

        authors = " ".join([a.get("family", "") for a in article.get("author", [])])

        year = str(article["issued"]["date-parts"][0][0])

        return f"{title} {authors} {year}".strip()[: self.max_query_len]

    def score(self, article, preprint, strategies):
        scores = [s.score(article, preprint) for s in strategies]
        return sum(scores) / len(scores)

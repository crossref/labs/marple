import json
import re
import urllib.parse

from matching.utils import crossref_rest_api_call
from strategies_available.preprint_sbmv.strategy import Strategy as SBMV
from strategies_available.preprint_ml.strategy import Strategy as ML


class Strategy:
    strategy = "preprint-ensemble-cascade"
    task = "preprint-matching"
    description = (
        "This strategy uses ML-based strategy first, and if there are "
        + "no results, SBMV is used next."
    )
    default = False

    max_query_len = 5000

    def match(self, input_data):
        if input_data.startswith("10."):
            _, article = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(input_data)}", {}
            )
        else:
            article = json.loads(input_data)

        candidates = self.get_candidates(article)

        matched = ML().match_candidates(article, candidates)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        if matched:
            return matched

        matched = SBMV().match_candidates(article, candidates)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        return matched

    def get_candidates(self, article):
        query = self.candidate_query(article)
        code, results = crossref_rest_api_call(
            "works", {"query.bibliographic": query, "filter": "type:posted-content"}
        )
        return [r for r in results["items"] if r.get("subtype", "") == "preprint"]

    def candidate_query(self, article):
        title = " ".join(article["title"])
        title = re.sub("mml:[a-z]*", "", title)
        title = re.sub("&amp;", "&", title)
        title = re.sub("&lt;", "<", title)
        title = re.sub("&gt;", ">", title)

        authors = " ".join([a.get("family", "") for a in article.get("author", [])])

        year = str(article["issued"]["date-parts"][0][0])

        return f"{title} {authors} {year}".strip()[: Strategy.max_query_len]

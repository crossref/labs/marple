from strategies_available.affiliation_multi_search.matching import match_affiliation


class Strategy:
    strategy = "affiliation-multi-search"
    task = "affiliation-matching"
    description = "Heuristic-based strategy; it segments the affiliation string, performs multiple candidate searches against ES and uses a number of heuristics to validate the candidates."
    default = True

    def match(self, input_data):
        matched = match_affiliation(input_data)
        if not matched:
            return []
        matched = matched[0]
        if not matched.chosen:
            return []

        return [
            {
                "id": matched.organization["id"],
                "confidence": matched.score,
                "strategies": [self.strategy],
            }
        ]

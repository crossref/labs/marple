from matching.search import ES_CLIENT

from opensearch_dsl import Search, Q


class ESQueryBuilder:
    def __init__(self):
        self.search = Search(using=ES_CLIENT, index="organization")
        self.search = self.search.params(search_type="dfs_query_then_fetch")

    def add_string_query(self, terms):
        self.search = self.search.query(
            "nested",
            path="names",
            score_mode="max",
            query=Q("query_string", query=terms, fuzzy_max_expansions=1),
        )

    def add_phrase_query(self, terms):
        self.search = self.search.query(
            "nested",
            path="names",
            score_mode="max",
            query=Q("match_phrase", **{"names.name": terms}),
        )

    def add_common_query(self, terms):
        self.search = self.search.query(
            "nested",
            path="names",
            score_mode="max",
            query=Q(
                "common", **{"names.name": {"query": terms, "cutoff_frequency": 0.001}}
            ),
        )

    def add_fuzzy_query(self, terms):
        self.search = self.search.query(
            "nested",
            path="names",
            score_mode="max",
            query=Q("match", **{"names.name": {"query": terms, "fuzziness": "AUTO"}}),
        )

    def get_query(self):
        return self.search

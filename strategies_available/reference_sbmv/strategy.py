import json


from strategies_available.reference_sbmv_unstructured.strategy import (
    Strategy as SBMVUnstructuredStrategy,
)
from strategies_available.reference_parsed_doi.strategy import (
    Strategy as ParsedDOIStrategy,
)
from strategies_available.reference_parsed_isbn.strategy import (
    Strategy as ParsedISBNStrategy,
)


class Strategy:
    strategy = "reference-sbmv"
    task = "reference-matching"
    description = "Combines strategies parsed-doi and sbmv-unstructured."
    default = True

    def match(self, input_data):
        matched = ParsedDOIStrategy().match(input_data)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        if matched:
            return matched

        matched = ParsedISBNStrategy().match(input_data)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        if matched:
            return matched

        try:
            data = json.loads(input_data)
            matched_u = []
            if "unstructured-citation" in data:
                matched_u = SBMVUnstructuredStrategy().match(
                    data["unstructured-citation"]
                )
            str_input = ""
            for field in [
                "author",
                "journal-title",
                "volume",
                "issue",
                "first-page",
                "year",
                "series-title",
                "volume-title",
                "proceedings-title",
                "article-title",
            ]:
                str_input += " "
                str_input += data.get(field, "")
            matched_s = SBMVUnstructuredStrategy().match(str_input)
            matched = matched_u + matched_s
            matched = sorted(matched, key=lambda m: m["confidence"], reverse=True)
            for item in matched:
                item["strategies"] = [type(self).__name__] + item["strategies"]
            return matched[:1] if matched else matched
        except json.decoder.JSONDecodeError:
            pass

        matched = SBMVUnstructuredStrategy().match(input_data)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        return matched

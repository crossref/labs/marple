import re
import unidecode

from rapidfuzz import fuzz

from matching.utils import crossref_rest_api_call, doi_id


class Strategy:
    strategy = "reference-sbmv-unstructured"
    task = "reference-matching"
    description = (
        "SBMV uses Crossref's REST API to search for candidate works, "
        + "and compares the metadata to validate the candiates and make "
        + "the final decision."
    )
    default = False

    def match(self, input_data):
        ref_string = input_data
        ref_string = re.sub(
            r"(?<!\d)10\.\d{4,9}/[-\._;\(\)/:a-zA-Z0-9]+", "", ref_string
        )
        ref_string = re.sub(r"(?<![a-zA-Z0-9])arXiv:[\d\.]+", "", ref_string)
        ref_string = re.sub(r"\[[^\[\]]*\]", "", ref_string)
        ref_string = re.sub("Retrieved.*http.*", "", ref_string).strip()

        _, results = crossref_rest_api_call(
            "works", {"query.bibliographic": ref_string}
        )
        results = results["items"]
        if results is None or not results:
            return []

        candidates = self.select_candidates(ref_string, results)

        doi, score = self.choose_best(candidates, ref_string, 0.34)

        if doi is None:
            return []
        return [
            {
                "id": doi_id(doi),
                "confidence": score,
                "strategies": [self.strategy],
            }
        ]

    def select_candidates(self, ref_string, results):
        candidates = []
        for result in results:
            if not candidates:
                candidates.append(result)
            elif result.get("score") / len(ref_string) >= 0.4:
                candidates.append(result)
            else:
                break
        return candidates

    def choose_best(self, candidates, ref_string, min_similarity):
        if not candidates:
            return None, None
        similarities = [self.similarity(c, ref_string) for c in candidates]
        if max(similarities) < min_similarity:
            return None, None
        return candidates[similarities.index(max(similarities))].get("DOI"), max(
            similarities
        )

    def similarity(self, candidate, ref_string):
        # weights for generalized jaccard similarity
        cand_set = {}
        str_set = {}

        # weights of relevalnce score
        cand_set["score"] = candidate["score"] / 100
        str_set["score"] = max(1, candidate["score"] / 100)

        # weights of normalized relevance score
        cand_set["score_norm"] = candidate["score"] / len(ref_string)
        str_set["score_norm"] = max(1, candidate["score"] / len(ref_string))

        # complete last page if abbreviated
        # changes "1425-37" to "1425-1437"
        for pages in re.findall(
            r"\d+[\u002D\u00AD\u2010\u2011\u2012\u2013"
            + r"\u2014\u2015\u207B\u208B\u2212-]\d+",
            ref_string,
        ):
            numbers = re.findall(r"(?<!\d)\d+(?!\d)", pages)
            first = numbers[0]
            last = numbers[1]
            if len(first) > len(last) and int(first[-len(last) :]) <= int(last):
                last = first[: (len(first) - len(last))] + last
                ref_string = re.sub(pages, first + "-" + last, ref_string)

        # all number appearing in the ref string
        ref_numbers = re.findall(r"(?<!\d)\d+(?!\d)", ref_string[5:])
        ref_numbers = [re.sub("^0+", "", n) if len(n) > 1 else n for n in ref_numbers]
        if not ref_numbers:
            return 0

        # if volume equals year, but only one instance is present
        # in the reference string, add another copy
        issued = candidate["issued"]["date-parts"]
        if (
            "volume" in candidate
            and issued is not None
            and issued[0][0] is not None
            and candidate["volume"] == str(issued[0][0])
            and ref_numbers.count(candidate["volume"]) == 1
        ):
            ref_numbers.append(candidate["volume"])

        # weights of volume
        if "volume" in candidate:
            self.update_weights_all(
                "volume", candidate["volume"], ref_numbers, cand_set, str_set
            )

        # weights for year
        if issued is not None and issued[0][0] is not None:
            self.update_weights_all(
                "year", str(issued[0][0]), ref_numbers, cand_set, str_set
            )

        # weights for issue
        if "issue" in candidate:
            self.update_weights_all(
                "issue", candidate["issue"], ref_numbers, cand_set, str_set
            )

        # weights for pages
        if "page" in candidate:
            self.update_weights_all(
                "page", candidate["page"], ref_numbers, cand_set, str_set
            )

        # weights for article number
        if "article-number" in candidate:
            self.update_weights_all(
                "article-number",
                candidate["article-number"],
                ref_numbers,
                cand_set,
                str_set,
            )

        # weights for title
        if "title" in candidate and candidate["title"]:
            self.update_weights_all(
                "title", candidate["title"][0], ref_numbers, cand_set, str_set
            )

        # weights for container-title
        if "container-title" in candidate and candidate["container-title"]:
            self.update_weights_all(
                "ctitle",
                candidate["container-title"][0],
                ref_numbers,
                cand_set,
                str_set,
            )

        # weights for author
        if (
            "author" in candidate
            and candidate["author"]
            and ("family" in candidate["author"][0] or "name" in candidate["author"][0])
        ):
            author = candidate["author"][0]
            a = unidecode.unidecode(author.get("family", author.get("name"))).lower()
            b = unidecode.unidecode(ref_string).lower()[: (5 * len(a))]
            cand_set["author"] = 1
            str_set["author"] = fuzz.partial_ratio(a, b) / 100
        elif (
            "editor" in candidate
            and candidate["editor"]
            and ("family" in candidate["editor"][0] or "name" in candidate["editor"][0])
        ):
            editor = candidate["editor"][0]
            a = unidecode.unidecode(editor.get("family", editor.get("name"))).lower()
            b = unidecode.unidecode(ref_string).lower()[: (5 * len(a))]
            cand_set["author"] = 1
            str_set["author"] = fuzz.partial_ratio(a, b) / 100

        # if the year wasn't found, try with year +- 1
        if issued is not None and issued[0][0] is not None and str_set["year_0"] == 0:
            if str(issued[0][0] - 1) in ref_numbers:
                str_set["year_0"] = 0.5
                ref_numbers.remove(str(issued[0][0] - 1))
            elif str(issued[0][0] + 1) in ref_numbers:
                str_set["year_0"] = 0.5
                ref_numbers.remove(str(issued[0][0] + 1))

        support = 0
        if "title" in candidate and candidate["title"]:
            a = unidecode.unidecode(candidate["title"][0]).lower()
            b = unidecode.unidecode(ref_string).lower()
            if fuzz.partial_ratio(a, b) / 100 > 0.75:
                support = support + 1
        for k, v in str_set.items():
            if k == "year_0" and v > 0:
                support = support + 1
            if k == "volume_0" and v == 1:
                support = support + 1
            if k == "author" and v > 0.7:
                support = support + 1
            if k == "page_0" and v == 1:
                support = support + 1
            if (
                candidate.get("type", "") == "proceedings-article"
                and k.startswith("ctitle")
                and v == 1
            ):
                support = support + 1
        if candidate.get("type", "") == "journal-article":
            if support < 4:
                return 0
        else:
            if support < 3:
                return 0

        # weights for the remaining numbers in the ref string
        for i, r in enumerate(ref_numbers):
            cand_set["rest_" + str(i)] = 0
            str_set["rest_" + str(i)] = 1

        # generalized Jaccard similarity
        num = sum([min(cand_set[n], str_set[n]) for n in cand_set.keys()])
        den = sum([max(cand_set[n], str_set[n]) for n in cand_set.keys()])
        if den == 0:
            return 1
        return num / den

    def get_cand_norm(self, candidate, name):
        return unidecode.unidecode(candidate.get(name, [""])[0]).lower().strip()

    def get_ref_norm(self, ref, name):
        return unidecode.unidecode(ref.get(name, "")).lower().strip()

    def update_weights_all(
        self, name, cand_str, ref_numbers, cand_set, str_set, weight=1
    ):
        for i, number in enumerate(re.findall(r"(?<!\d)\d+(?!\d)", cand_str)):
            cand_set[name + "_" + str(i)] = weight
            str_set[name + "_" + str(i)] = 0
            number = re.sub("^0+", "", number)
            if number in ref_numbers:
                str_set[name + "_" + str(i)] = weight
                ref_numbers.remove(number)

import re

from matching.utils import crossref_rest_api_call, doi_id


class Strategy:
    strategy = "reference-parsed-isbn"
    task = "reference-matching"
    description = "This strategy uses regexes to locate the ISBN in the input string."
    default = False

    def match(self, input_data):
        regex = r"(978-?|979-?)?[\dxX](-?[\dxX]){9}"
        isbn_strings = set()
        for isbn_string in re.finditer(regex, input_data):
            isbn_string = re.sub(r"[^\w]", "", isbn_string.group())
            isbn_strings.add(isbn_string)
        isbn_strings = list(isbn_strings)
        for isbn_string in isbn_strings:
            code, response = crossref_rest_api_call(
                "works/", {"filter": f"isbn:{isbn_string},type:book"}
            )
            if len(response["items"]) == 1:
                return [
                    {
                        "id": doi_id(response["items"][0]["DOI"]),
                        "confidence": 1.0,
                        "strategies": [self.strategy],
                    }
                ]
        return []

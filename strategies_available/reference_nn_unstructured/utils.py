import re

from rapidfuzz import fuzz
from trax import layers as tl


def clean_ref(ref):
    ref = re.sub(r"(?<!\d)10\.\d{4,9}/[-\._;\(\)/:a-zA-Z0-9]+", "", ref)
    ref = re.sub(r"(?<![a-zA-Z0-9])arXiv:[\d\.]+", "", ref)
    ref = re.sub(r"\[[^\[\]]*\]", "", ref)
    return re.sub("Retrieved.*http.*", "", ref).strip()


def typ(item):
    return item.get("type", "")


def title(item):
    title = " ".join(item.get("title", []))
    return re.sub(r"\s+", " ", title)


def subtitle(item):
    title = " ".join(item.get("subtitle", []))
    return re.sub(r"\s+", " ", title)


def ctitle(item):
    title = " ".join(item.get("container-title", []))
    return re.sub(r"\s+", " ", title)


def score(item):
    return item["score"]


def volume(item):
    return item.get("volume", "")


def issue(item):
    return item.get("issue", "")


def page(item):
    return item.get("page", "")


def year(item):
    issued = item["issued"]["date-parts"]
    if not issued:
        return ""
    if issued[0][0] is None:
        return ""
    return str(issued[0][0])


def number(item):
    return item.get("article-number", "")


def authors(item):
    authors = item.get("author", [])
    authors = [a.get("family", a.get("name", "")) for a in authors]
    authors = [a for a in authors if a]
    authors += [""] * 5
    return authors[:5]


def editors(item):
    authors = item.get("editor", [])
    authors = [a.get("family", a.get("name", "")) for a in authors]
    authors = [a for a in authors if a]
    authors += [""] * 5
    return authors[:5]


def extract_metadata(item):
    return (
        [
            score(item),
            typ(item),
            title(item),
            subtitle(item),
            ctitle(item),
            year(item),
            volume(item),
            issue(item),
            page(item),
            number(item),
        ]
        + authors(item)
        + editors(item)
    )


def extract_numbers(s):
    return re.findall(r"(?<!\d)\d+(?!\d)", s)


def partial_similarity(s1, s2):
    if not s1 or not s2:
        return 0.0
    return fuzz.partial_ratio(s1.lower(), s2.lower()) / 100.0


def contains_number(s, numbers):
    s = extract_numbers(s)
    if not s:
        return 0.0
    return s[0] in numbers


def extract_features(data_point):
    (
        ref,
        score,
        typ,
        title,
        subtitle,
        ctitle,
        year,
        volume,
        issue,
        page,
        number,
        author1,
        author2,
        author3,
        author4,
        author5,
        editor1,
        editor2,
        editor3,
        editor4,
        editor5,
    ) = data_point
    year = str(int(year)) if year else year
    number = str(int(number)) if isinstance(number, float) else number

    ref = clean_ref(ref)

    ref_numbers = extract_numbers(ref[5:])
    ref_numbers = [re.sub("^0+", "", n) if len(n) > 1 else n for n in ref_numbers]

    page_numbers = extract_numbers(page)

    contrib1 = author1 or editor1
    contrib2 = author2 or editor2
    contrib3 = author3 or editor3
    contrib4 = author4 or editor4
    contrib5 = author5 or editor5

    return [
        min(score / 120.0, 1.0),
        min(score / len(ref), 1.0),
        partial_similarity(ref, title),
        partial_similarity(ref, subtitle),
        partial_similarity(ref, ctitle),
        partial_similarity(ref, contrib1),
        partial_similarity(ref, contrib2),
        partial_similarity(ref, contrib3),
        partial_similarity(ref, contrib4),
        partial_similarity(ref, contrib5),
        contains_number(year, ref_numbers),
        contains_number(volume, ref_numbers),
        contains_number(issue, ref_numbers),
        contains_number(number, ref_numbers),
        contains_number(page_numbers[0], ref_numbers) if len(page_numbers) > 0 else 0,
        contains_number(page_numbers[1], ref_numbers) if len(page_numbers) > 1 else 0,
        1 if typ == "journal-article" else 0,
        1 if typ == "book-chapter" else 0,
        1 if typ == "proceedings-article" else 0,
        1 if typ == "book" else 0,
        (
            1
            if typ
            not in ["journal-article", "book-chapter", "proceedings-article", "book"]
            else 0
        ),
    ]


def create_model():
    return tl.Serial(tl.Dense(n_units=16), tl.Dense(n_units=2), tl.LogSoftmax())

import numpy as np
import strategies_available.reference_nn_unstructured.utils as nn_utils

from matching.utils import crossref_rest_api_call, doi_id


class Strategy:
    strategy = "reference-nn-unstructured"
    task = "reference-matching"
    description = ""
    default = False

    def __init__(self):
        self.model = nn_utils.create_model()
        self.model.init_from_file(
            file_name="strategies_available/reference_nn_unstructured/model/model.pkl.gz",
            weights_only=True,
        )

    def match(self, input_data):
        ref_string = nn_utils.clean_ref(input_data)

        _, results = crossref_rest_api_call(
            "works", {"query.bibliographic": ref_string, "rows": 50}
        )
        candidates = results["items"]
        if candidates is None or not candidates:
            return []

        doi, score = self.choose_best(candidates, ref_string)

        if doi is None:
            return []
        return [
            {
                "id": doi_id(doi),
                "confidence": score,
                "strategies": [self.strategy],
            }
        ]

    def choose_best(self, candidates, ref_string):
        best_score = -1000
        best_doi = None

        batches = [candidates[i : i + 10] for i in range(0, 50, 10)]
        batches = [b for b in batches if b]
        for batch in batches:
            features = []
            dois = []
            for item in batch:
                data = [ref_string] + nn_utils.extract_metadata(item)
                features.append(nn_utils.extract_features(data))
                dois.append(item["DOI"])
            features = np.array(features)
            response = self.model(features)
            for d, r in zip(dois, response):
                if r[1] > r[0] and r[1] > best_score:
                    best_score = r[1]
                    best_doi = d.lower()

        if best_doi is None:
            return None, None
        return best_doi, np.exp(best_score)

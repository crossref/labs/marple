import argparse
import numpy as np
import os
import pandas as pd
import utils as nn_utils
import trax

from trax import layers as tl
from trax.supervised import training


def get_batches(data, size=64):
    batch = []
    output = []
    i = 0
    while True:
        if i >= len(data):
            i = 0
        if len(batch) == size:
            yield np.array(batch), np.array(output)
            batch = []
            output = []

        features = nn_utils.extract_features(data[i][:-1])
        output.append(data[i][-1])
        batch.append(features)
        i += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Training a model")
    parser.add_argument(
        "-i", "--input", help="input data file", type=str, required=True
    )
    parser.add_argument(
        "-n", "--iterations", help="training iterations", type=int, default=2000
    )
    parser.add_argument(
        "-o", "--output", help="output directory", type=str, required=True
    )
    args = parser.parse_args()

    data = pd.read_csv(args.input, header=None)
    data = data.fillna("")

    train_size = int(0.8 * len(data))
    data_train = np.array(data[:train_size])
    data_val = np.array(data[train_size:])
    print("Training data shape", data_train.shape)
    print("Validation data shape", data_val.shape)
    del data

    model = nn_utils.create_model()

    output_dir = os.path.expanduser(args.output)

    train_task = training.TrainTask(
        labeled_data=get_batches(data_train),
        loss_layer=tl.CategoryCrossEntropy(),
        optimizer=trax.optimizers.Adam(learning_rate=0.01),
    )

    eval_task = training.EvalTask(
        labeled_data=get_batches(data_val),
        metrics=[tl.CategoryCrossEntropy(), tl.CategoryAccuracy()],
    )

    training_loop = training.Loop(
        model, train_task, eval_tasks=[eval_task], output_dir=output_dir
    )
    training_loop.run(args.iterations)

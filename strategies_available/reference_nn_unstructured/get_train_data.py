import argparse
import csv
import json
import random
import requests
import utils as nn_utils

from time import sleep


def get_data_points(ref_string, doi):
    doi = doi[16:]
    candidates = requests.get(
        "https://api.crossref.org/works",
        {"query.bibliographic": ref_string, "rows": 50},
    )
    candidates = candidates.json()["message"]["items"]
    positive = [c for c in candidates if c["DOI"] == doi]
    if not positive:
        return None, None
    positive = positive[0]
    negative = [c for c in candidates if c["DOI"] != doi][0]
    return nn_utils.extract_metadata(positive), nn_utils.extract_metadata(negative)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Extract training dataset from samples"
    )
    parser.add_argument("-i", "--input", help="input file", type=str, required=True)
    parser.add_argument("-s", "--size", help="dataset size", type=int, default=-1)
    parser.add_argument("-o", "--output", help="output file", type=str, required=True)
    args = parser.parse_args()

    data = []
    with open(args.input, "r") as f:
        data = json.load(f)["items"]
    data = [d for d in data if d["output"]]

    random.seed(1)
    random.shuffle(data)

    written = 0
    with open(args.output, "w") as f:
        writer = csv.writer(f)
        for item in data:
            ref = item["input"]
            doi = item["output"][0]
            try:
                pos, neg = get_data_points(ref, doi)
            except requests.RequestException:
                sleep(10)
                continue
            if pos is None or neg is None:
                continue
            writer.writerow([ref] + pos + [1])
            writer.writerow([ref] + neg + [0])
            written += 1
            if written % 100 == 0:
                print("written", written, "references")
            if written == args.size:
                break

import re
import torch

from strategies_available.affiliation_multi_search.strategy import (
    Strategy as StrategyRor,
)

from transformers import BertTokenizerFast, BertForTokenClassification

from unidecode import unidecode
from gensim.parsing.preprocessing import (
    preprocess_string,
    strip_tags,
    strip_punctuation,
    strip_multiple_whitespaces,
)


class Strategy:
    strategy = "affiliation-multi-search-ner"
    task = "affiliation-matching"
    description = "This strategy runs affiliation-multi-search strategy first, and if it doesn't return anything, the organisation name is extracted from the affiliation string with a NER model and affiliation-multi-search strategy is attempted again."
    default = False

    def __init__(self):
        self.model = BertForTokenClassification.from_pretrained(
            "adambuttrick/ner-test-bert-base-uncased-finetuned-500K-AdamW-3-epoch-locations"
        )
        self.model = self.model.cpu()
        self.tokenizer = BertTokenizerFast.from_pretrained("bert-base-uncased")

    def match(self, input_data):
        matched = StrategyRor().match(input_data)
        if matched:
            return matched
        affiliation = self.normalize_input_data(input_data)
        inputs = self.preprocess_input(self.tokenizer, affiliation)
        ids, flattened_predictions = self.ner_inference(self.model, inputs)
        prediction = self.postprocess_output(
            self.tokenizer, ids, flattened_predictions, inputs
        )
        if not prediction:
            return []
        joined_prediction = self.construct_string(affiliation, prediction)
        matched = StrategyRor().match(joined_prediction)
        for item in matched:
            item["strategies"] = [self.strategy] + item["strategies"]
        return matched

    def normalize_input_data(self, input_data):
        custom_filters = [
            lambda x: x.lower(),
            strip_tags,
            strip_punctuation,
            strip_multiple_whitespaces,
        ]
        input_data = unidecode(" ".join(preprocess_string(input_data, custom_filters)))
        return input_data.split()

    def preprocess_input(self, tokenizer, affiliation):
        inputs = tokenizer(
            affiliation,
            is_split_into_words=True,
            return_offsets_mapping=True,
            padding="max_length",
            truncation=True,
            max_length=128,
            return_tensors="pt",
        )
        return inputs

    def ner_inference(self, model, inputs):
        ids = inputs["input_ids"]
        mask = inputs["attention_mask"]
        outputs = model(ids, attention_mask=mask)
        logits = outputs[0]
        active_logits = logits.view(-1, model.num_labels)
        flattened_predictions = torch.argmax(active_logits, axis=1)
        return ids, flattened_predictions

    def postprocess_output(self, tokenizer, ids, flattened_predictions, inputs):
        ids_to_labels = {0: "B-ORG", 1: "I-ORG", 2: "O", 3: "B-LOC", 4: "I-LOC"}
        tokens = tokenizer.convert_ids_to_tokens(ids.squeeze().tolist())
        token_predictions = [
            ids_to_labels[i] for i in flattened_predictions.cpu().numpy()
        ]
        wp_preds = list(zip(tokens, token_predictions))
        prediction = []
        for token_pred, mapping in zip(
            wp_preds, inputs["offset_mapping"].squeeze().tolist()
        ):
            if mapping[0] == 0 and mapping[1] != 0:
                prediction.append(token_pred[1])
            else:
                continue
        invalid_tag_combinations = (
            all(tag in ["I-ORG", "B-LOC", "I-LOC", "O"] for tag in prediction)
            and "B-ORG" not in prediction
        )
        if invalid_tag_combinations:
            return None
        return prediction

    def construct_string(self, affiliation, prediction):
        entities = []
        temp_entity = []
        tags_and_strings = {}
        for i in range(len(prediction)):
            tag = prediction[i]
            tags_and_strings[i] = f"{tag}: {affiliation[i]}"
            if tag in ["B-ORG", "I-ORG", "B-LOC", "I-LOC"]:
                if temp_entity and tag in ["B-ORG", "B-LOC"]:
                    entities.append(" ".join(temp_entity))
                    temp_entity = []

                temp_entity.append(affiliation[i])
            else:
                if temp_entity:
                    entities.append(" ".join(temp_entity))
                    temp_entity = []
        if temp_entity:
            entities.append(" ".join(temp_entity))
        entities = [re.sub(",", "", entity) for entity in entities]
        joined_prediction = ", ".join(entities)
        return joined_prediction

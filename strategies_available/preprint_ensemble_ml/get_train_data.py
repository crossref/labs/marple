import argparse
import csv
import json
import random
import re
import requests
import sys

try:
    sys.path.append(".")
except IndexError:
    pass

from strategies_available.preprint_sbmv.strategy import Strategy as SBMVPreprintStrategy
from strategies_available.preprint_ml.strategy import Strategy as MLPreprintStrategy


def candidate_query(article):
    title = " ".join(article["title"])
    title = re.sub("mml:[a-z]*", "", title)
    title = re.sub("&amp;", "&", title)
    title = re.sub("&lt;", "<", title)
    title = re.sub("&gt;", ">", title)

    authors = " ".join([a.get("family", "") for a in article.get("author", [])])

    year = str(article["issued"]["date-parts"][0][0])

    return f"{title} {authors} {year}".strip()[:5000]


def get_data_points(article, preprints, sbmv_strategy, nn_strategy):
    preprints = set([p[16:] for p in preprints])
    query = candidate_query(article)
    candidates = requests.get(
        "https://api.crossref.org/works",
        {"query.bibliographic": query, "filter": "type:posted-content", "rows": 50},
    )
    candidates = candidates.json()["message"]["items"]
    candidates = [c for c in candidates if c.get("subtype", "") == "preprint"]
    sbmv_scores = [sbmv_strategy.score(article, c) for c in candidates]
    nn_scores = [float(nn_strategy.score(article, c)) for c in candidates]
    for i, candidate in enumerate(candidates):
        candidate["rank"] = i + 1
    positive = [
        [c["rank"], sbmv_scores, nn_scores] for c in candidates if c["DOI"] in preprints
    ]
    negative = [
        [c["rank"], sbmv_scores, nn_scores]
        for c in candidates
        if c["DOI"] not in preprints
    ][:1]
    return positive, negative


def write_data(data, fn, sbmv_strategy, nn_strategy):
    written = 0
    with open(fn, "w") as f:
        writer = csv.writer(f)
        for item in data:
            article = json.loads(item["input"])
            preprints = item["output"]
            pos, neg = get_data_points(article, preprints, sbmv_strategy, nn_strategy)
            for p in pos:
                writer.writerow([p[0], json.dumps(p[1]), json.dumps(p[2]), 1])
            for n in neg:
                writer.writerow([n[0], json.dumps(n[1]), json.dumps(n[2]), 0])
            written += 1
            if written % 10 == 0:
                print("written", written, "data points")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Extract training dataset from samples"
    )
    parser.add_argument("-i", "--input", help="input file", type=str, required=True)
    parser.add_argument(
        "-s", "--size", help="train subset size", type=int, required=True
    )
    parser.add_argument(
        "-t", "--training-output", help="training output file", type=str, required=True
    )
    parser.add_argument(
        "-v",
        "--validation-output",
        help="validation output file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-dt",
        "--dataset-training-output",
        help="dataset training output file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-dv",
        "--dataset-validation-output",
        help="dataset validation output file",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    with open(args.input, "r") as f:
        dataset = json.load(f)

    random.seed(1)
    random.shuffle(dataset["items"])
    items_train = dataset["items"][: args.size]
    items_validation = dataset["items"][args.size :]

    with open(args.dataset_training_output, "w") as f:
        dataset["items"] = items_train
        json.dump(dataset, f, indent=2)
    with open(args.dataset_validation_output, "w") as f:
        dataset["items"] = items_validation
        json.dump(dataset, f, indent=2)

    sbmv_strategy = SBMVPreprintStrategy()
    nn_strategy = MLPreprintStrategy()

    write_data(items_train, args.training_output, sbmv_strategy, nn_strategy)
    write_data(items_validation, args.validation_output, sbmv_strategy, nn_strategy)

import json
import numpy as np
import re
import urllib.parse
import strategies_available.preprint_ensemble_ml.utils as ensemble_utils

from matching.utils import crossref_rest_api_call, doi_id

from strategies_available.preprint_sbmv.strategy import Strategy as SBMV
from strategies_available.preprint_ml.strategy import Strategy as ML


class Strategy:
    strategy = "preprint-ensemble-ml"
    task = "preprint-matching"
    description = (
        "This strategy combines SBMV and ML-based strategies into "
        + "another ML-based model."
    )
    default = False

    min_score = 0.93
    max_query_len = 5000

    def __init__(self):
        self.model = ensemble_utils.create_model()
        self.model.init_from_file(
            file_name="strategies_available/preprint_ensemble_ml/model/model.pkl.gz",
            weights_only=True,
        )

    def match(self, input_data):
        if input_data.startswith("10."):
            _, article = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(input_data)}", {}
            )
        else:
            article = json.loads(input_data)

        query = self.candidate_query(article)
        code, results = crossref_rest_api_call(
            "works", {"query.bibliographic": query, "filter": "type:posted-content"}
        )
        candidates = [r for r in results["items"] if r.get("subtype", "") == "preprint"]
        if not candidates:
            return []

        sbmv_strategy = SBMV()
        ml_strategy = ML()
        scores_sbmv = [sbmv_strategy.score(article, r) for r in candidates]
        scores_ml = [float(ml_strategy.score(article, r)) for r in candidates]

        features = [
            [r + 1, json.dumps(scores_sbmv), json.dumps(scores_ml)]
            for r in range(len(candidates))
        ]
        features = ensemble_utils.extract_features(features)
        log_prob = self.model(features)
        prob = np.exp(log_prob)
        prob = list(p[1] for p in prob)

        matches = [
            (d["DOI"], s) for d, s in zip(candidates, prob) if s >= self.min_score
        ]
        return [
            {
                "id": doi_id(d),
                "confidence": s,
                "strategies": [self.strategy],
            }
            for d, s in matches
        ]

    def candidate_query(self, article):
        title = " ".join(article["title"])
        title = re.sub("mml:[a-z]*", "", title)
        title = re.sub("&amp;", "&", title)
        title = re.sub("&lt;", "<", title)
        title = re.sub("&gt;", ">", title)

        authors = " ".join([a.get("family", "") for a in article.get("author", [])])

        year = str(article["issued"]["date-parts"][0][0])

        return f"{title} {authors} {year}".strip()[: self.max_query_len]

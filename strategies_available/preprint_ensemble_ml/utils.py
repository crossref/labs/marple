import json
import numpy as np

from trax import layers as tl


def extract_features(data_points):
    ranks = [p[0] for p in data_points]
    sbmv_scores = [json.loads(p[1]) for p in data_points]
    ml_scores = [json.loads(p[2]) for p in data_points]
    max_sbmv = [max(s) for s in sbmv_scores]
    max_ml = [max(s) for s in ml_scores]
    larger_sbmv = [
        len([s for s in sc if s > sc[r - 1]]) for r, sc in zip(ranks, sbmv_scores)
    ]
    larger_ml = [
        len([s for s in sc if s > sc[r - 1]]) for r, sc in zip(ranks, ml_scores)
    ]

    features = np.array(
        [
            [s[r - 1] for r, s in zip(ranks, sbmv_scores)],
            [m - s[r - 1] for r, s, m in zip(ranks, sbmv_scores, max_sbmv)],
            max_sbmv,
            [min(n, 10) / 10 for n in larger_sbmv],
            [s[r - 1] for r, s in zip(ranks, ml_scores)],
            [m - s[r - 1] for r, s, m in zip(ranks, ml_scores, max_ml)],
            max_ml,
            [min(n, 10) / 10 for n in larger_ml],
        ]
    )

    return features.T


def create_model():
    return tl.Serial(tl.Dense(n_units=16), tl.Dense(n_units=2), tl.LogSoftmax())

class Strategy:
    strategy = "reference-cs-combined"
    task = "reference-matching"
    description = (
        "Strategy used in CS on deposits. Here only for evaluation "
        + "purposes, no code is available."
    )
    default = False

    def match(self, input_data):
        raise NotImplementedError

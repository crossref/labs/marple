import json
import re

from matching.utils import crossref_rest_api_call, doi_id


funders = {}
with open("strategies/funders-data.jsonl", "r") as f:
    for funder_data in f:
        funder = json.loads(funder_data)
        funders[funder["DOI"]] = funder

for funder in funders.values():
    for alias in funder["aliases"]:
        if funder["DOI"] not in funders[alias]["aliases"]:
            funders[alias]["aliases"].append(funder["DOI"])

for funder in funders.values():
    ans = []
    for alias in funder["aliases"]:
        ans.extend(funders[alias]["names"])
    funder["aliases-names"] = list(set(ans))


for funder in funders.values():
    dns = []
    for desc in funder["descendants"]:
        dns.extend(funders[desc]["names"])
    funder["family-names"] = list(set(dns))

for funder in funders.values():
    for desc in funder["descendants"]:
        funders[desc]["family-names"].extend(funder["names"])
        funders[desc]["family-names"] = list(set(funders[desc]["family-names"]))


class Strategy:
    def funder_doi_norm(self, doi):
        if not doi:
            return ""
        return "10.13039/" + re.sub(r".*\/", "", doi)

    def are_aliases(self, funder_doi, funder_dois):
        if funder_doi not in funders:
            return False
        funder_dois = [doi for doi in funder_dois if doi and doi in funders]
        for doi in funder_dois:
            if (
                doi in funders[funder_doi]["aliases"]
                or funder_doi in funders[doi]["aliases"]
            ):
                return True
        return False

    def are_family(self, funder_doi, funder_dois):
        if funder_doi not in funders:
            return False
        funder_dois = [doi for doi in funder_dois if doi and doi in funders]
        for doi in funder_dois:
            if (
                doi in funders[funder_doi]["descendants"]
                or funder_doi in funders[doi]["descendants"]
            ):
                return True
        return False

    def name_matches(self, funder_name, funder_dois, name="names"):
        for funder_doi in funder_dois:
            if funder_name.lower() in funders[funder_doi][name]:
                return True
        return False

    def grant_funder_dois(self, grant):
        return set(
            [
                self.funder_doi_norm(fid["id"]).lower()
                for project in grant["project"]
                for funding in project["funding"]
                for fid in funding["funder"]["id"]
            ]
        )

    def match_by_grant_doi(self, award):
        status, grant_by_doi = crossref_rest_api_call(f"works/{award}", {})
        if status != 200:
            return None
        return {"id": doi_id(grant_by_doi["DOI"]), "confidence": 1}

    def match_by_award_and_funder_doi(self, funder_doi, grant):
        if funder_doi in self.grant_funder_dois(grant):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.95}
        if self.are_aliases(funder_doi, self.grant_funder_dois(grant)):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.9}
        if self.are_family(funder_doi, self.grant_funder_dois(grant)):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.85}
        return None

    def match_by_award_and_funder_name(self, funder_name, grant):
        if self.name_matches(funder_name, self.grant_funder_dois(grant)):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.8}
        if self.name_matches(
            funder_name, self.grant_funder_dois(grant), name="aliases-names"
        ):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.75}
        if self.name_matches(
            funder_name, self.grant_funder_dois(grant), name="family-names"
        ):
            return {"id": doi_id(grant["DOI"]), "confidence": 0.7}
        return None

    def match(self, input_data):
        input_object = json.loads(input_data)
        if "award" not in input_object:
            return []
        matched = []
        for award in input_object["award"]:
            match_grant_doi = self.match_by_grant_doi(award)
            if match_grant_doi is not None:
                matched.append(match_grant_doi)
                continue

            _, candidates = crossref_rest_api_call(
                "types/grant/works", {"filter": "award.number:" + award}
            )
            candidates = candidates["items"]
            for candidate in candidates:
                if "DOI" in input_object:
                    match_funder_doi = self.match_by_award_and_funder_doi(
                        input_object["DOI"], candidate
                    )
                    if match_funder_doi is not None:
                        matched.append(match_funder_doi)
                else:
                    match_funder_name = self.match_by_award_and_funder_name(
                        input_object["name"], candidate
                    )
                    if match_funder_name is not None:
                        matched.append(match_funder_name)

        for item in matched:
            item["strategies"] = [type(self).__name__]
        return matched

import re
import urllib.parse

from matching.utils import crossref_rest_api_call, doi_id


class Strategy:
    strategy = "reference-parsed-doi"
    task = "reference-matching"
    description = "This strategy uses regexes to locate the DOI in the input string."
    default = False

    def match(self, input_data):
        doi_strings = set()
        for doi_string in re.finditer(r"(?=(10\.[\d]+/[^ ]+))", input_data):
            doi_string = doi_string.group(1)
            doi_strings.add(doi_string)
            modified = doi_string
            while True:
                candidate = re.sub(r"[^\w]$", "", modified)
                if candidate == modified:
                    break
                doi_strings.add(candidate)
                modified = candidate
        doi_strings = list(doi_strings)
        doi_strings.sort(key=lambda s: -len(s))
        for doi_string in doi_strings:
            code, _ = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(doi_string)}", {}
            )
            if code == 200:
                return [
                    {
                        "id": doi_id(doi_string),
                        "confidence": 1.0,
                        "strategies": [self.strategy],
                    }
                ]
        return []

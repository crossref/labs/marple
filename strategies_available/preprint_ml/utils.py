import json
import math
import numpy as np
import re

from rapidfuzz import fuzz
from trax import layers as tl


def authors_score(article, preprint):
    article_authors = article.get("author", []).copy()
    preprint_authors = preprint.get("author", []).copy()
    sum_len = len(article_authors) + len(preprint_authors)
    if sum_len > 70:
        article_author_names = " ".join(
            [a.get("family", a.get("name", "")) for a in article_authors]
        )
        preprint_author_names = " ".join(
            [a.get("family", a.get("name", "")) for a in preprint_authors]
        )
        return (
            fuzz.token_sort_ratio(
                article_author_names.lower(), preprint_author_names.lower()
            )
            / 100
        )

    score = 0.0
    while article_authors and preprint_authors:
        s, i, j = most_similar_pair(article_authors, preprint_authors)
        del article_authors[i]
        del preprint_authors[j]
        score += s
    return 2 * score / sum_len if sum_len else 0.5


def most_similar_pair(authors1, authors2):
    best_score = 0.0
    index1 = 0
    index2 = 0
    for i1, a1 in enumerate(authors1):
        for i2, a2 in enumerate(authors2):
            score = score_author_similarity(a1, a2)
            if score > best_score:
                best_score = score
                index1 = i1
                index2 = i2
            if best_score > 0.99:
                return best_score, index1, index2
    return best_score, index1, index2


def score_author_similarity(a1, a2):
    if "ORCID" in a1 and "ORCID" in a2:
        return 1.0 if a1["ORCID"] == a2["ORCID"] else 0.0
    best_score = 0.0
    for n1 in author_names(a1):
        for n2 in author_names(a2):
            score = fuzz.ratio(n1, n2) / 100
            if score > best_score:
                best_score = score
    return best_score


def author_names(author):
    n = author.get("name", "").lower()
    gi = author.get("given", "").lower()
    g = re.sub(r" .*", "", gi)
    f = author.get("family", "").lower()
    names = [n, f"{gi} {f}", f"{f} {gi}", f"{g} {f}", f"{f} {g}"]
    return set([name.strip() for name in names if name.strip()])


def year_score(article, preprint):
    article_year = article["issued"]["date-parts"][0][0]
    preprint_year = preprint["issued"]["date-parts"][0][0]
    return 1 / (1 + math.pow(math.e, preprint_year - article_year))


def title_score(article, preprint, sim_fun):
    at = article["title"][0].lower().strip() if article["title"] else ""
    pt = preprint["title"][0].lower().strip() if preprint["title"] else ""
    score = sim_fun(at, pt) / 100

    def differ_by_keywords(title1, title2):
        two_words_1 = " ".join(title1.split()[:2])
        two_words_2 = " ".join(title2.split()[:2])
        for keyword in ["correction", "response", "reply", "appendix"]:
            if (keyword in two_words_1 and keyword not in title2) or (
                keyword not in title1 and keyword in two_words_2
            ):
                return True
        return False

    if differ_by_keywords(at, pt):
        score /= 1.5
    return score


def extract_features(data_points):
    articles = [json.loads(p[0]) for p in data_points]
    preprints = [json.loads(p[1]) for p in data_points]

    features = np.array(
        [
            [authors_score(a, p) for a, p in zip(articles, preprints)],
            [title_score(a, p, fuzz.ratio) for a, p in zip(articles, preprints)],
            [
                title_score(a, p, fuzz.partial_ratio)
                for a, p in zip(articles, preprints)
            ],
            [
                title_score(a, p, fuzz.token_sort_ratio)
                for a, p in zip(articles, preprints)
            ],
            [year_score(a, p) for a, p in zip(articles, preprints)],
        ]
    )

    return features.T


def create_model():
    return tl.Serial(tl.Dense(n_units=16), tl.Dense(n_units=2), tl.LogSoftmax())

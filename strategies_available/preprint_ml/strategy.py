import json
import numpy as np
import re
import urllib.parse
import strategies_available.preprint_ml.utils as ml_utils

from matching.utils import crossref_rest_api_call, doi_id


class Strategy:
    strategy = "preprint-ml"
    task = "preprint-matching"
    description = (
        "This strategy uses Crossref's REST API to search for candidate "
        + "preprints, and an ML-based model makes pairwise match/no match "
        + "decisions."
    )
    default = False

    min_score = 0.65
    max_score_diff = 0.01
    max_query_len = 5000

    def __init__(self):
        self.model = ml_utils.create_model()
        self.model.init_from_file(
            file_name="strategies_available/preprint_ml/model/model.pkl.gz",
            weights_only=True,
        )

    def match(self, input_data):
        if input_data.startswith("10."):
            _, article = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(input_data)}", {}
            )
        else:
            article = json.loads(input_data)

        candidates = self.get_candidates(article)

        return self.match_candidates(article, candidates)

    def get_candidates(self, article):
        query = self.candidate_query(article)
        code, results = crossref_rest_api_call(
            "works", {"query.bibliographic": query, "filter": "type:posted-content"}
        )
        return [r for r in results["items"] if r.get("subtype", "") == "preprint"]

    def match_candidates(self, article, candidates):
        scores = [(r["DOI"], self.score(article, r)) for r in candidates]

        matches = [(d, s) for d, s in scores if s >= self.min_score]
        top_score = max([0] + [s for _, s in matches])
        matches = [(d, s) for d, s in matches if top_score - s < self.max_score_diff]

        return [
            {
                "id": doi_id(d),
                "confidence": s,
                "strategies": [self.strategy],
            }
            for d, s in matches
        ]

    def candidate_query(self, article):
        title = " ".join(article["title"])
        title = re.sub("mml:[a-z]*", "", title)
        title = re.sub("&amp;", "&", title)
        title = re.sub("&lt;", "<", title)
        title = re.sub("&gt;", ">", title)

        authors = " ".join([a.get("family", "") for a in article.get("author", [])])

        year = str(article["issued"]["date-parts"][0][0])

        return f"{title} {authors} {year}".strip()[: self.max_query_len]

    def score(self, article, preprint):
        features = ml_utils.extract_features(
            [[json.dumps(article), json.dumps(preprint)]]
        )
        log_prob = self.model(features)
        prob = np.exp(log_prob)
        return prob[0][1]

import argparse
import numpy as np
import os
import pandas as pd
import utils as nn_utils
import trax

from trax import layers as tl
from trax.supervised import training


def get_batches(data, size=64, max_data_points=None):
    returned = 0
    batch = []
    output = []
    i = 0
    while True:
        if max_data_points is not None and returned + len(output) >= max_data_points:
            break
        if i >= len(data):
            i = 0
        if len(batch) == size:
            yield nn_utils.extract_features(batch), np.array(output)
            batch = []
            output = []
            returned += size

        output.append(data[i][-1])
        batch.append(data[i][:-1])
        i += 1
    if len(batch) > 0:
        yield nn_utils.extract_features(batch), np.array(output)


def eval(data_val, max_data_points, threshold):
    batches = get_batches(data_val, 16, max_data_points=max_data_points)
    results = []
    for features, true_output in batches:
        log_probs = model(features)
        probs = np.exp(log_probs)
        pred_output = probs[:, 1] > threshold
        for t, p in zip(true_output, pred_output):
            r = (t, 1 if p else 0)
            results.append(r)
    precision = (
        len([t for t, p in results if t and t == p]) / len([t for t, p in results if p])
        if len([t for t, p in results if p])
        else 1.0
    )
    recall = (
        len([t for t, p in results if t and t == p]) / len([t for t, p in results if t])
        if len([t for t, p in results if t])
        else 1.0
    )
    f05 = (1.0 + 0.5 * 0.5) * precision * recall / (0.5 * 0.5 * precision + recall)
    print("Threshold:", threshold)
    print("Precision:", precision)
    print("Recall:", recall)
    print("F0.5:", f05)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a model")
    parser.add_argument(
        "-t", "--training", help="training data file", type=str, required=True
    )
    parser.add_argument(
        "-v", "--validation", help="validation data file", type=str, required=True
    )
    parser.add_argument(
        "-i", "--iterations", help="training iterations", type=int, default=2000
    )
    parser.add_argument(
        "-o", "--output", help="output model directory", type=str, required=True
    )
    args = parser.parse_args()

    data_train = np.array(pd.read_csv(args.training, header=None))
    data_val = np.array(pd.read_csv(args.validation, header=None))
    print("Training data shape", data_train.shape)
    print("Validation data shape", data_val.shape)

    model = nn_utils.create_model()
    print(model)

    output_dir = os.path.expanduser(args.output)

    train_task = training.TrainTask(
        labeled_data=get_batches(data_train),
        loss_layer=tl.CategoryCrossEntropy(),
        optimizer=trax.optimizers.Adam(learning_rate=0.01),
    )

    eval_task = training.EvalTask(
        labeled_data=get_batches(data_val),
        metrics=[tl.CategoryCrossEntropy(), tl.CategoryAccuracy()],
    )

    training_loop = training.Loop(
        model, train_task, eval_tasks=[eval_task], output_dir=output_dir
    )
    training_loop.run(args.iterations)

    threshold = 0.8
    while threshold < 1.0:
        print()
        eval(data_val, len(data_val), threshold)
        threshold += 0.02

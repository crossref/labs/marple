import argparse
import csv
import json
import random
import re
import requests


def copy_fields(d, fields):
    copy = {}
    for field in fields:
        if field in d:
            copy[field] = d[field]
    return copy


def article_representation(metadata):
    representation = copy_fields(
        metadata,
        [
            "DOI",
            "score",
            "references-count",
            "language",
            "title",
            "subtitle",
            "short-title",
            "original-title",
            "issued",
        ],
    )
    if "author" in metadata:
        representation["author"] = [
            copy_fields(author, ["name", "given", "family", "ORCID"])
            for author in metadata["author"]
        ]
    return representation


def candidate_query(article):
    title = " ".join(article["title"])
    title = re.sub("mml:[a-z]*", "", title)
    title = re.sub("&amp;", "&", title)
    title = re.sub("&lt;", "<", title)
    title = re.sub("&gt;", ">", title)

    authors = " ".join([a.get("family", "") for a in article.get("author", [])])

    year = str(article["issued"]["date-parts"][0][0])

    return f"{title} {authors} {year}".strip()[:5000]


def get_data_points(query, preprints):
    preprints = set([p[16:] for p in preprints])
    candidates = requests.get(
        "https://api.crossref.org/works",
        {"query.bibliographic": query, "filter": "type:posted-content", "rows": 50},
    )
    candidates = candidates.json()["message"]["items"]
    candidates = [c for c in candidates if c.get("subtype", "") == "preprint"]
    positive = [c for c in candidates if c["DOI"] in preprints]
    negative = [c for c in candidates if c["DOI"] not in preprints][:1]
    return positive, negative


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Get training data from a dataset")
    parser.add_argument(
        "-d", "--dataset", help="input dataset file", type=str, required=True
    )
    parser.add_argument("-s", "--size", help="dataset size", type=int, default=-1)
    parser.add_argument("-o", "--output", help="output file", type=str, required=True)
    args = parser.parse_args()

    data = []
    with open(args.dataset, "r") as f:
        data = json.load(f)["items"]

    random.seed(1)
    random.shuffle(data)

    pos_sum = 0
    neg_sum = 0
    written = 0
    with open(args.output, "w") as f:
        writer = csv.writer(f)
        for item in data:
            article = json.loads(item["input"])
            query = candidate_query(article)
            preprints = item["output"]
            pos, neg = get_data_points(query, preprints)
            pos_sum += len(pos)
            neg_sum += len(neg)
            for p in pos:
                writer.writerow(
                    [
                        json.dumps(article_representation(article)),
                        json.dumps(article_representation(p)),
                        1,
                    ]
                )
            for n in neg:
                writer.writerow(
                    [
                        json.dumps(article_representation(article)),
                        json.dumps(article_representation(n)),
                        0,
                    ]
                )
            written += 1
            if written % 10 == 0:
                print("written", written, "data points", pos_sum, neg_sum)
            if written == args.size:
                break
    print(pos_sum, neg_sum)

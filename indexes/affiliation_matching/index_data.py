import argparse
import boto3
import json

from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth


INDEX = "organization"


def get_auth():
    credentials = boto3.Session().get_credentials()
    return AWSV4SignerAuth(credentials, "us-east-1", "es")


def get_names(org):
    for name in org["names"]:
        if "acronym" not in name["types"]:
            yield name["value"]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Index data for affiliation matching")
    parser.add_argument("-u", "--host", help="ES host", type=str, required=True)
    parser.add_argument(
        "-d", "--dump", help="ROR V2 data dump", type=str, required=True
    )
    args = parser.parse_args()

    es_client = OpenSearch(
        hosts=[{"host": args.host, "port": 443}],
        http_auth=get_auth(),
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )

    with open(args.dump, "r") as f:
        ror_dump = json.load(f)

    organizations = []
    for i, org in enumerate(ror_dump):
        if i % 1000 == 0:
            print(f"indexed {i} organizations")

        for rel in org["relationships"]:
            del rel["label"]
        org["primary"] = [
            n["value"] for n in org["names"] if "ror_display" in n["types"]
        ][0]
        names = [{"name": n} for n in get_names(org)]
        org["names"] = names
        org["country"] = org["locations"][0]["geonames_details"]["country_code"]
        for field in [
            "links",
            "types",
            "established",
            "external_ids",
            "admin",
            "domains",
            "locations",
        ]:
            del org[field]

        if len(organizations) >= 400:
            bulk_data = "\n".join(organizations)
            res = es_client.bulk(bulk_data)
            if res["errors"]:
                print(res)
            organizations = []
        organizations.append(
            json.dumps({"index": {"_index": "organization", "_id": org["id"]}})
        )
        organizations.append(json.dumps(org))

    bulk_data = "\n".join(organizations)
    res = es_client.bulk(bulk_data)
    if res["errors"]:
        print(res)

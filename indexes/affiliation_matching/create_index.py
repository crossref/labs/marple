import argparse
import boto3

from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth
from opensearchpy.exceptions import NotFoundError

INDEX = "organization"


def get_auth():
    credentials = boto3.Session().get_credentials()
    return AWSV4SignerAuth(credentials, "us-east-1", "es")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create ES index for affiliation matching"
    )
    parser.add_argument("-u", "--host", help="ES host", type=str, required=True)
    args = parser.parse_args()

    es_client = OpenSearch(
        hosts=[{"host": args.host, "port": 443}],
        http_auth=get_auth(),
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )

    try:
        response = es_client.indices.delete(index=INDEX)
        print(response)
    except NotFoundError:
        print("index not found")

    index_body = {
        "settings": {
            "number_of_shards": 1,
            "analysis": {
                "analyzer": {
                    "string_lowercase": {
                        "tokenizer": "standard",
                        "filter": ["lowercase", "asciifolding"],
                    }
                },
            },
        },
        "mappings": {
            "properties": {
                "id": {"type": "keyword"},
                "country": {"type": "keyword"},
                "status": {"type": "keyword"},
                "primary": {"type": "text", "analyzer": "string_lowercase"},
                "names": {
                    "type": "nested",
                    "properties": {
                        "name": {"type": "text", "analyzer": "string_lowercase"},
                    },
                },
                "relationships": {
                    "properties": {
                        "type": {"type": "keyword"},
                        "id": {"type": "keyword"},
                    }
                },
            }
        },
    }

    response = es_client.indices.create(INDEX, body=index_body)
    print(response)

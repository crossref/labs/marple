from opensearchpy import AWSV4SignerAuth, OpenSearch, RequestsHttpConnection, helpers

import boto3
import json
import os
import logging
import datetime

logger = logging.getLogger(__name__)
logging.basicConfig(
    format="%(asctime)s %(levelname)s: %(message)s",
    encoding="utf-8",
    level=logging.INFO,
)


def get_auth():
    credentials = boto3.Session().get_credentials()
    return AWSV4SignerAuth(credentials, "us-east-1", "es")


# SQS queue URL
queue_url = os.environ.get("PREPRINT_QUEUE", "preprint-queue")

# S3 bucket
bucket_name = os.environ.get("PREPRINT_BUCKET", "render-bucket")

# Preprint index name
opensearch_index = os.environ.get("ES_PREPRINT_INDEX", "preprint")

# Initialize SQS client
sqs = boto3.client("sqs")
# Initialize S3 client
s3 = boto3.client("s3")

if os.environ.get("ES_HOST"):
    es_client = OpenSearch(
        hosts=[{"host": os.environ.get("ES_HOST"), "port": 443}],
        http_auth=get_auth(),
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )
elif os.environ.get("ES_HOST_DEV"):
    es_client = OpenSearch(
        hosts=[
            {
                "host": os.environ.get("ES_HOST_DEV"),
                "port": int(os.environ.get("ES_PORT", "9200")),
            }
        ],
        use_ssl=False,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )


def fetch_file_from_s3(filename):
    try:
        # Get file content from S3
        response = s3.get_object(Bucket=bucket_name, Key=filename)
        body = response["Body"].read().decode("utf-8")
        return json.loads(body)
    except Exception as e:
        logger.error(f"Error fetching file {filename} from S3: {e}")
        return None


def parse_response(response):
    data = []
    key_receipt_dict = {}

    if "Messages" in response:
        for message in response["Messages"]:
            # Parse S3 notification message
            s3_notification = json.loads(message["Body"])
            message_body = json.loads(s3_notification["Message"])
            if "Records" in message_body:
                s3_object_key = message_body["Records"][0]["s3"]["object"]["key"]

                # is it a preprint?
                if s3_object_key.endswith("/13"):
                    logger.info(f"Processing {s3_object_key}")
                    # Fetch file from S3
                    document = fetch_file_from_s3(s3_object_key)
                    if document:
                        logger.info(f"Fetched file {s3_object_key} from S3")

                        _id = s3_object_key.split("/")[0]
                        document["_index"] = opensearch_index
                        document["_id"] = _id
                        data.append(document)
                        key_receipt_dict[_id] = message["ReceiptHandle"]
                    else:
                        logger.error(f"Failed to fetch file {s3_object_key} from S3")
                else:
                    logger.info(f"Skipping {s3_object_key}")
                    sqs.delete_message(
                        QueueUrl=queue_url, ReceiptHandle=message["ReceiptHandle"]
                    )
    return data, key_receipt_dict


def post_to_opensearch(data):
    succeeded = []
    failed = []

    try:
        for success, item in helpers.parallel_bulk(es_client, actions=data):
            if success:
                succeeded.append(item)
            else:
                failed.append(item)
    except Exception as exc:
        logger.error(exc, stack_info=True, exc_info=True)

    return succeeded, failed


def process_messages_from_sqs():
    # Receive messages from SQS queue
    response = sqs.receive_message(
        QueueUrl=queue_url, MaxNumberOfMessages=10, WaitTimeSeconds=20
    )

    # Prepare the data.
    # The key_receipt_dict helps keep track of the receiptHandle for each key.
    data, key_receipt_dict = parse_response(response)

    # Post and track successes and failures
    succeeded, failed = post_to_opensearch(data)

    for item in succeeded:
        # Delete message from SQS queue
        receipt_handle = key_receipt_dict[item["index"]["_id"]]
        sqs.delete_message(QueueUrl=queue_url, ReceiptHandle=receipt_handle)
        logger.info(f"Message posted to Opensearch successfully: {item}")

    for item in failed:
        logger.error(
            f"Failed to post message to Opensearch for S3 key : {item['index']['_id']}"
        )
        logger.error(f'Error: { item["index"]["error"]}')


if __name__ == "__main__":
    with open("heartbeat", "w") as heartbeat:
        while True:
            # process the queue
            process_messages_from_sqs()

            # log a heartbeat
            current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            heartbeat.seek(0)
            heartbeat.write(f"{current_time}\n")
            heartbeat.truncate()

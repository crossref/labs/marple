#!/bin/bash

# Currently this only works for the marple indexer
if [ "$APPLICATION" != "indexer" ]; then    
    exit 0
fi

# Read the content of the file
heartbeat=$(<"heartbeat")

# Convert file content to Unix timestamp
heartbeat_timestamp=$(date -d "$heartbeat" +"%s")

# Get the current time in Unix timestamp
current_timestamp=$(date +"%s")

# Calculate the difference in seconds
time_diff=$((current_timestamp - heartbeat_timestamp))

# Check if the time difference is greater than 45 seconds
if [ "$time_diff" -gt 45 ]; then
    exit 1
else    
    exit 0
fi

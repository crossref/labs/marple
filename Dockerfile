FROM public.ecr.aws/docker/library/python:3.11

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . code
WORKDIR /code

EXPOSE 8000

HEALTHCHECK --interval=45s --timeout=10s --start-period=60s --retries=1 CMD ["./healthcheck.sh"]

CMD ["bash", "docker-entrypoint.sh"]
